﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
	private Transform player;
	private Rigidbody2D body;
	public int speed;	
	void Start ()
	{
		body = GetComponent<Rigidbody2D>(); 
		player = GameObject.Find("Player").transform;
        if (!GameManager.instance.first)
            transform.position = new Vector3(GameManager.instance.newCamXpos, GameManager.instance.newCamYpos, -10);

	}

	void Update ()
	{
		Vector2 force = new Vector2(-(transform.position.x-player.position.x)*speed,-(transform.position.y-player.position.y)*speed);
        body.velocity = force;
    }
}
