﻿using UnityEngine;
using System.Collections;

public class TriggerPortal : MonoBehaviour
{

    public float newPointX;
    public float newPointY;
    public float newCameraX;
    public float newCameraY;

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            runPlayer(hit.transform);
        if (hit.gameObject.tag.Equals("Enemy"))
            runEnemy(hit.transform);
    }

    void OnTriggerStay2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            runPlayer(hit.transform);
        if (hit.gameObject.tag.Equals("Enemy"))
            runEnemy(hit.transform);
    }

    private void runPlayer(Transform player)
    {
        GameObject camera = GameObject.Find("Main Camera");
        player.transform.position = new Vector3(newPointX, newPointY, player.transform.position.z);
        camera.transform.position = new Vector3(newCameraX, newCameraY, camera.transform.position.z);
        print(player.position.y);
    }
    private void runEnemy(Transform Enemy)
    {
        Enemy.transform.position = new Vector3(newPointX, newPointY, Enemy.transform.position.z);
    }
}
