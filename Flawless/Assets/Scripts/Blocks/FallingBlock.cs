﻿using UnityEngine;
using System.Collections;

public class FallingBlock : MonoBehaviour 
{
    public int Timer;
    private int counter;
    private Rigidbody2D body;
	void Start ()
    {
        counter = 0;
	    body = gameObject.GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        if (counter != 0)
        {
            if (counter == Timer)
                DestroyObject(this.gameObject);
            counter++;
        }
    }
    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            body.isKinematic = false;
            counter++;
        }
    }
}
