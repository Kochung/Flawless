﻿using UnityEngine;
using System.Collections;

public class Moveable
{
    private Rigidbody2D body;
    private float forceX;
    private bool once;
    private float maxYVelocity;

    public Moveable(Rigidbody2D Body)
    {
        body = Body;
        maxYVelocity = 0;
    }
    public Moveable(Rigidbody2D Body,float YVelocity)
    {
        body = Body;
        maxYVelocity = YVelocity;
    }
    public void move(float x, float y)
    {
        body.velocity = new Vector2(x, y);
    }
    public void moveX(float x)
    {
        body.velocity = new Vector2(x, body.velocity.y);
    }
    public void addX(float x)
    {
        body.velocity = new Vector2(body.velocity.x+x, body.velocity.y);
    }
    public void moveY(float y)
    {
        body.velocity = new Vector2(body.velocity.x, y);
    }
    public void CheckY()
    {
        if (maxYVelocity != 0 &&body.velocity.y < -maxYVelocity)
            moveY(-maxYVelocity);
    }
    public void ForceX()
    {
        if (once)
        {
            body.velocity = new Vector2(forceX, body.velocity.y);
            once = false;
        }
    }
    public void setForceX(float amount)
    {
        forceX = amount;
        once = true;
    }
    public void flip(Transform transform)
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
