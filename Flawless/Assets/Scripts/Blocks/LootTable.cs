﻿using UnityEngine;
using System.Collections;

public class LootTable : MonoBehaviour
{
    public GameObject[] Drops;
    public int [] probability;
    private int amount;
    public void open ()
    { 
        amount = 0;
	    for(int i=0;i<probability.Length; i++)
        {
            amount += probability[i];
            probability[i] = amount;
        }
	}
	
    public void Drop(float x,float y)
    {
        int number = GameManager.instance.random.Next(amount);
        int index = 0;
        for (int i = 0; i < probability.Length; i++)
        {
            if(number < probability[i])
            {
                index = i;
                break;
            }
        }
        if(Drops[index]!=null)
            Instantiate(Drops[index], new Vector3(x, y, 0), Quaternion.identity);
    }
}
