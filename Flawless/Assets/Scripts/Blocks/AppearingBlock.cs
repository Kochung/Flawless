﻿using UnityEngine;
using System.Collections;

public class AppearingBlock : MonoBehaviour
{
    SpriteRenderer Image;
	// Use this for initialization
	void Start ()
    {
        Image = transform.GetChild(1).GetComponent<SpriteRenderer>();
        Image.enabled = false;    
	}

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            Image.enabled = true;
        }
    }
}
