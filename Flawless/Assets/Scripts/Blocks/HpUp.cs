﻿using UnityEngine;
using System.Collections;

public class HpUp : MonoBehaviour {

    public int index;
    public int hpUp;
    void Start ()
    {
        if (GameManager.instance.GottenItem[index])
            DestroyObject(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            GameManager.instance.GottenItem[index] = true;
            DestroyObject(this.gameObject);
            GameManager.instance.addHp(hpUp);
        }
    }
}
