﻿using UnityEngine;
using System.Collections;
using System;

public class Pot : MonoBehaviour, HitAble
{
    public int index;
    private HurtBox hurtBox;

    void Start ()
    {
        if (index != -1 && GameManager.instance.GottenItem[index])
        {
            GetComponent<LootTable>().Drop(transform.position.x, transform.position.y);
            DestroyObject(this.gameObject);
        }
        hurtBox = transform.GetChild(0).GetComponent<HurtBox>();
        hurtBox.setParent(this);
        GetComponent<LootTable>().open();
    }

    public void gotHit(Collision2D hit)
    {
        if(hit.gameObject.tag.Equals("Player"))
        {
            if(index!=-1)
                GameManager.instance.GottenItem[index] = true;
            GetComponent<LootTable>().Drop(transform.position.x,transform.position.y);
            DestroyObject(this.gameObject);
        }
    }
    
    void HitAble.HitConnected(Collision2D hit){}
}
