﻿using UnityEngine;
using System.Collections;

public class BossKillDoor : MonoBehaviour
{

    public float newPointX;
    public float newPointY;
    public float newCameraX;
    public float newCameraY;
    public string LevelName;
    public string SongName;

    private bool active;

    void Update()
    {
        if (Time.timeScale == 0) return;
        if (active && Input.GetButtonDown("Up"))
            run();
    }

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            active = true;
            if (Input.GetButtonDown("Up"))
                run();
        }
    }

    void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            active = false;
    }

    private void run()
    {
        GameObject player = GameObject.Find("Player");
        GameManager.instance.PlayerFlip = player.GetComponent<Player>().getFlip();
        GameManager.instance.PlayerHp = player.GetComponent<Player>().getHP();
        GameManager.instance.newXpos = newPointX;
        GameManager.instance.newYpos = newPointY;
        GameManager.instance.newCamXpos = newCameraX;
        GameManager.instance.newCamYpos = newCameraY;
        if (GameManager.instance.bossDead)
        {
            GameManager.instance.newXpos = -1.962f;
            GameManager.instance.newYpos = -2.378f;
            GameManager.instance.newCamXpos = -7.76f;
            GameManager.instance.newCamYpos = 0f;
            GameManager.instance.Load("EndGameTeaser", "ground");
        }
        else
            GameManager.instance.Load(LevelName, SongName);
    }
}
