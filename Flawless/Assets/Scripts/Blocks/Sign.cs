﻿using UnityEngine;
using System.Collections;

public class Sign : MonoBehaviour
{
    [Multiline]
    public string[] message;

    public int [] fontSize;

    private bool active;

    void Update()
    {
        if (Time.timeScale == 0) return;
        if (active && Input.GetButtonDown("Up"))
            run();
    }

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            active = true;
            if (Input.GetButtonDown("Up"))
                run();
        }
    }

    void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            active = false;
    }

    private void run()
    {
        if(fontSize.Length != message.Length)
            GameManager.instance.sendTextBoxMessage(message);
        else
            GameManager.instance.sendTextBoxMessage(message,fontSize);

    }
}
