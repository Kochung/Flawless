﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameSettings : MonoBehaviour
{
    public string gameName;
    public float musicVolume = 0.2f;
    public bool muteMusic = false;
    public float sfxVolume = 1f;
    public bool muteSfx = false;
    public string difficulty;
    public bool hardcore;

    public static GameSettings instance = null;

    public bool cutScene = false;
    public int cutSceneRunning = 0;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {/*
        ChangeMusicVolume(musicVolume);
        ToggleMusic(muteMusic);
        ChangeSfxVolume(sfxVolume);
        ToggleSfx(muteSfx);*/
    }

    public void ChangeMusicVolume(float volume)
    {
        SoundManager.instance.musicSource.volume = musicVolume = volume;
    }

    public void ToggleMusic(bool mute)
    {
        SoundManager.instance.musicSource.mute = muteMusic = mute;
    }

    public void ChangeSfxVolume(float volume)
    {
        SoundManager.instance.sfxSource.volume = sfxVolume = volume;
    }

    public void ToggleSfx(bool mute)
    {
        SoundManager.instance.sfxSource.mute = muteSfx = mute;
    }
}
