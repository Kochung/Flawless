﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextBox : MonoBehaviour
{
    private SpriteRenderer box;
    private Text text;
    private Canvas canvas;
    private SpriteRenderer arrow;
    private int messageIndex;
    private int messagesIndex;
    private int messagelength;
    private int messageslength;
    private int counter;
    private bool Active;
    private bool Paused;
    private bool OneFrameStop;
    private bool noPause;

    public int speed;
    public int defaultFontSize;
    public string [] messages;
    public int[] fontSizes;

    void Start ()
    {
        canvas = GetComponent<Canvas>();
        box = transform.GetChild(0).GetComponent<SpriteRenderer>();
        text = transform.GetChild(1).GetComponent<Text>();
        arrow = transform.GetChild(2).GetComponent<SpriteRenderer>();
        canvas.enabled = false;
        box.enabled = false;
        arrow.enabled=false;
        text.text = "";
	}

    void Update()
    {
        if (Active && !Paused)
        {
            if(!noPause)
                Time.timeScale = 0;
            if (Input.GetButtonDown("Jump"))
            {
                if (messageIndex != messagelength)
                {
                    messageIndex = messagelength;
                    text.text = messages[messagesIndex];
                }
                else if (messagesIndex != messageslength - 1)
                {
                    messagesIndex++;
                    messageIndex = 0;
                    messagelength = messages[messagesIndex].Length + 1;
                    if (fontSizes.Length!=0 && fontSizes[messagesIndex] != 0)
                        text.fontSize = fontSizes[messagesIndex];
                    counter = 0;
                }
                else
                {
                    canvas.enabled = false;
                    box.enabled = false;
                    text.text = "";
                    messageIndex = 0;
                    messagelength = 0;
                    messagesIndex = 0;
                    messageslength = 0;
                    Active = false;
                    Time.timeScale = 1;
                    GameManager.instance.Unpause();
                    arrow.enabled = false;

                }
            }
            if (messageIndex != messagelength)
            {
                if (counter == 0)
                {
                    text.text = messages[messagesIndex].Substring(0, messageIndex);
                    counter++;
                }
                else
                {
                    counter++;
                    if (counter == speed)
                    {
                        messageIndex++;
                        counter = 0;
                    }
                }
            }
            if(box.enabled)
                arrow.enabled = (messageIndex == messagelength)&&(messagesIndex != messageslength - 1);
        }
        if(OneFrameStop)
        {
            OneFrameStop = false;
            Paused = false;
        }
	}
    public void SetMessage(string [] Messages)
    {
        messages = Messages;
        fontSizes = new int[0];
        messageIndex = 0;
        messagesIndex = 0;
        messagelength = messages[messagesIndex].Length+1;
        messageslength = messages.Length;
        counter = 0;
        canvas.enabled = true;
        box.enabled = true;
        Active = true;
        text.fontSize = defaultFontSize;
        noPause = false;
    }
    public void SetMessage(string[] Messages, int[] FontSize)
    {
        messages = Messages;
        fontSizes = FontSize;
        messageIndex = 0;
        messagesIndex = 0;
        messagelength = messages[messagesIndex].Length + 1;
        messageslength = messages.Length;
        counter = 0;
        canvas.enabled = true;
        box.enabled = true;
        Active = true;
        if (fontSizes[messagesIndex] != 0)
            text.fontSize = fontSizes[messagesIndex];
        else
            text.fontSize = defaultFontSize;
        noPause = false;
    }
    public void SetMessageNoPause(string[] Messages)
    {
        messages = Messages;
        fontSizes = new int[0];
        messageIndex = 0;
        messagesIndex = 0;
        messagelength = messages[messagesIndex].Length + 1;
        messageslength = messages.Length;
        counter = 0;
        canvas.enabled = true;
        box.enabled = true;
        Active = true;
        text.fontSize = defaultFontSize;
        noPause = true;
    }
    public void Pause()
    {
        Paused = true;
    }
    public void Unpause()
    {
        OneFrameStop = true;
    }
    public bool isActive()
    {
        return Active;
    }
}
