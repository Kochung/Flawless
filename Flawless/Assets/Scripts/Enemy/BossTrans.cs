﻿using UnityEngine;
using System.Collections;

public class BossTrans : MonoBehaviour
{

    private SpriteRenderer transSpriteRender;
    private Animator transAnimator;
    void Start()
    {
        transSpriteRender = GetComponent<SpriteRenderer>();
        transAnimator = GetComponent<Animator>();
    }

    public void transOff()
    {
        transAnimator.SetBool("Run", false);
        transSpriteRender.enabled = false;
    }
}
