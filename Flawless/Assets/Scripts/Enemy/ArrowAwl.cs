﻿using UnityEngine;
using System.Collections;

public class ArrowAwl : MonoBehaviour,HitAble
{


    private Rigidbody2D body;
    private Animator animator;
    private SpriteRenderer spriterenderer;
    private Transform player;
    private Moveable moveable;
    private HitBox hitBox2;
    private HurtBox hurtBox;
    private BoxCollider2D box;

    private bool attaking;
    private int Hitstun;
    private bool active;
    private int invulnerable;
    private int flip;
    private int flipCooldown;
    private int arrowCooldown;

    public float distance;
    public float attackDistanceX;
    public float attackDistanceY;
    public float moveSpeed;
    public float jumpSpeed;
    public float Hp;
    public int invincibilityFrames;
    public GameObject ARROW;
    public float arrowXOffset;
    public float arrowYOffset;
    public float arrowSpeed;
    public int cooldown;


    void Start()
    {
        GetComponent<LootTable>().open();
        body = GetComponent<Rigidbody2D>();
        box = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
        spriterenderer = GetComponent<SpriteRenderer>();
        player = GameObject.Find("Player").transform;
        moveable = new Moveable(body);

        hurtBox = transform.GetChild(0).GetComponent<HurtBox>();
        hitBox2 = transform.GetChild(1).GetComponent<HitBox>();

        hurtBox.setParent(this);
        hitBox2.setParent(this);

        flipCooldown = 0;
        if (transform.localScale.x > 0)
            flip = -1;
        else
            flip = 1;
    }

    void FixedUpdate()
    {

        if (active)
        {
            if (Hitstun != 0)
            {
                runHitstun();
            }
            else if (attaking)
            {
                runAttack();
            }
            else
            {
                checkAttack();
                if (!attaking)
                    Move();
                Deactivate();
            }
        }
        else
        {
            if (Hitstun != 0)
            {
                runHitstun();
            }
            else
            {
                moveable.moveX(0);
                Activate();
            }
        }
        runInvulnerable();
        moveable.CheckY();
    }

    public void gotHit(Collision2D hit)
    {
        if (hit.gameObject.tag.Equals("Player") || hit.gameObject.tag.Equals("HitAll"))
        {
            if (invulnerable == 0)
            {
                animator.SetBool("Hit", true);
                runDamageCalcuation(hit);
                hitBox2.setEnable(false);
                AttackRecovery();
                AttackOver();
            }
        }
    }
    public void HitConnected(Collision2D hit) { }

    private void runDamageCalcuation(Collision2D hit)
    {
        HitBox gotHitBox = hit.gameObject.GetComponent<HitBox>();
        Hp -= gotHitBox.getDamage();
        if (Hp < 0)
            Hp = 0;
        int direction = 1;
        if (hit.gameObject.transform.position.x - transform.position.x < 0)
            direction = -1;
        moveable.moveY(gotHitBox.knockbackY);
        moveable.setForceX(gotHitBox.knockbackX * direction);
        Hitstun = gotHitBox.hitStun;
        if (Hp == 0)
        {
            Hitstun = 150;
            animator.SetBool("Dead", true);
            GetComponent<LootTable>().Drop(transform.position.x, transform.position.y);
            hurtBox.off();
        }
        animator.SetBool("Hitstun", true);
    }

    private bool Grounded()
    {
        float HalfWidth = ((transform.lossyScale.x * box.size.x) / 2.0f);
        return GroundRaycastCheck(0) || GroundRaycastCheck(HalfWidth) || GroundRaycastCheck(-HalfWidth);
    }
    private bool GroundRaycastCheck(float offset)
    {
        float HalfHeight = (transform.lossyScale.y * box.size.y) / 2.0f;
        RaycastHit2D[] hit = Physics2D.RaycastAll(new Vector2(transform.position.x + offset, transform.position.y - HalfHeight + .1f), Vector2.down, .15f);
        int size = hit.Length;
        for (int i = 0; i < size; i++)
        {
            if (hit[i].transform.tag.Equals("Ground"))
            {
                return true;
            }
        }
        return false;
    }


    private float getDistance()
    {
        return Mathf.Sqrt(Mathf.Pow(player.position.x - transform.position.x, 2) + Mathf.Pow((player.position.y - transform.position.y), 2));
    }
    private float getDistanceX()
    {
        return Mathf.Abs(player.position.x - transform.position.x);
    }
    private float getDistanceY()
    {
        return Mathf.Abs(player.position.y - transform.position.y);
    }
    private bool wallRaycastCheck()
    {
        RaycastHit2D[] hit = Physics2D.RaycastAll(new Vector2(transform.position.x, transform.position.y), Vector2.left, .1f);
        int size = hit.Length;
        for (int i = 0; i < size; i++)
        {
            if (hit[i].transform.tag.Equals("EnemyWall"))
            {
                return true;
            }
        }
        hit = Physics2D.RaycastAll(new Vector2(transform.position.x, transform.position.y + .28f), Vector2.right, .1f);
        size = hit.Length;
        for (int i = 0; i < size; i++)
        {
            if (hit[i].transform.tag.Equals("EnemyWall"))
            {
                return true;
            }
        }
        return false;
    }
    private void checkAttack()
    {
        if (arrowCooldown == 0)
        {
            if (flip == 1 && (getDistanceX() <= attackDistanceX && getDistanceY() <= attackDistanceY) && player.position.x > transform.position.x)
            {
                animator.SetBool("Attack", true);
                attaking = true;
            }
            else if (flip == -1 && (getDistanceX() <= attackDistanceX && getDistanceY() <= attackDistanceY) && player.position.x < transform.position.x)
            {
                animator.SetBool("Attack", true);
                attaking = true;
            }
        }
        else
            arrowCooldown--;
    }
    private void Move()
    {
        if ((wallRaycastCheck() || Mathf.Abs(body.velocity.x) < .5f) && flipCooldown == 0)
        {
            flip *= -1;
            moveable.flip(transform);
            flipCooldown = 10;
        }
        else if (flipCooldown != 0)
            flipCooldown--;
        moveable.moveX(flip * moveSpeed);
    }
    private void Deactivate()
    {
        if (getDistance() > distance)
        {
            animator.SetBool("Run", false);
            animator.SetBool("Attack", false);
            active = false;
        }
    }
    private void Activate()
    {
        if (getDistance() < distance)
        {
            if ((player.position.x < transform.position.x && flip == 1) || (player.position.x > transform.position.x && flip == -1))
            {
                flip *= -1;
                moveable.flip(transform);
            }
            animator.SetBool("Run", true);
            active = true;
            flipCooldown = 10;
        }
    }
    private void runHitstun()
    {
        animator.SetBool("Hit", false);
        if (Grounded())
            moveable.moveX(0);
        else
            moveable.ForceX();
        if (Hitstun == 1)
        {
            if (Hp <= 0)
            {
                DestroyObject(this.gameObject);
            }
            else
            {
                animator.SetBool("Hitstun", false);
                invulnerable = invincibilityFrames;
                hitBox2.setEnable(true);
                flipCooldown = 10;
            }
        }
        Hitstun--;
    }
    private void runAttack()
    {
        moveable.moveX(0);
    }
    private void runInvulnerable()
    {

        if (invulnerable != 0)
        {
            invulnerable--;
            if (invulnerable % 3 == 1)
                spriterenderer.enabled = false;
            else
                spriterenderer.enabled = true;
        }
    }

    public void AttackActive()
    {
        GameObject arrow = Instantiate(ARROW, new Vector3(transform.position.x + (arrowXOffset * flip), transform.position.y + arrowYOffset, transform.position.z), Quaternion.identity) as GameObject;
        Arrow TempArrow = arrow.GetComponent<Arrow>();
        TempArrow.start();
        TempArrow.Flip(flip);
        TempArrow.setSpeed(arrowSpeed);

    }
    public void AttackRecovery()
    {
    }
    public void AttackOver()
    {
        attaking = false;
        animator.SetBool("Attack", false);
        flipCooldown = 10;
        arrowCooldown = cooldown;
    }
}
