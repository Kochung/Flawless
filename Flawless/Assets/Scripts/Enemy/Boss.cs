﻿using UnityEngine;
using System.Collections;

public class Boss : MonoBehaviour, HitAble
{
    private Rigidbody2D body;
    private Animator animator;
    private SpriteRenderer spriterenderer;
    private Transform player;
    private Moveable moveable;
    private HitBox hitBox;
    private HitBox hitBox2;
    private HurtBox hurtBox;
    private BoxCollider2D box;
    private BoxCollider2D hitBoxCollider;
    private BoxCollider2D hurtBoxCollider;
    public GameObject BLOCKPattern;

    private SpriteRenderer transSpriteRender;
    private Animator transAnimator;

    private Vector2 Left = new Vector2(-14.47532f, -14.96508f);
    private Vector2 Right = new Vector2(-0.5900446f, -14.96508f);
    private Vector2 Mid; 

    private bool attaking;
    private int Hitstun;
    private int invulnerable;
    private int flip;
    private bool throwFlip=true;
    private int Pos = 1;
    private bool madeBlocks;
    private bool patternOver;
    private bool yellow;

    public int AttackType;
    public float moveSpeed;
    public float jumpSpeed;
    public float Hp;
    public int invincibilityFrames;
    public GameObject BOMB;
    public GameObject ARROW;
    public GameObject ARROWDOWN;
    public GameObject BLOCKPattern1;
    public GameObject BLOCKPattern2;
    public GameObject BLOCKPattern3;
    public float bombXOffset;
    public float bombYOffset;
    public float bombStartX;
    public float bombStartY;
    public int ArrowHold;

    public int hold;
    public int state;
    public int attackState;

    private int [] attackPattern;
    private int patternLength;
    private int attackIndex;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        box = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
        spriterenderer = GetComponent<SpriteRenderer>();
        player = GameObject.Find("Player").transform;
        moveable = new Moveable(body);
        
        transSpriteRender = transform.GetChild(2).GetComponent<SpriteRenderer>();
        transAnimator = transform.GetChild(2).GetComponent<Animator>();

        hurtBox = transform.GetChild(0).GetComponent<HurtBox>();
        hitBox2 = transform.GetChild(1).GetComponent<HitBox>();
        hitBox = transform.GetChild(3).GetComponent<HitBox>();

        hurtBoxCollider = transform.GetChild(0).GetComponent<BoxCollider2D>();
        hitBoxCollider = transform.GetChild(1).GetComponent<BoxCollider2D>();

        hurtBox.setParent(this);
        hitBox.setParent(this);
        hitBox2.setParent(this);

        hitBox.setEnable(false);

        if (transform.localScale.x > 0)
            flip = -1;
        else
            flip = 1;
        Mid = new Vector2((Left.x + Right.x)/2f, -14.96508f);

        state = 0;
        hold = 300;
        AttackType = -1;
    }

    void FixedUpdate()
    {
        moveable.moveX(0);
        if (Hitstun != 0)
        {
            runHitstun();
        }
        else if (Hp == 0)
        {
            RunDeathStates();
        }
        else if (attaking)
        {
            runAttack();
        }
        else
        {
            runStates();
        }
        runInvulnerable();
        moveable.CheckY();
    }

    public void gotHit(Collision2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            if (invulnerable == 0)
            {
                runDamageCalcuation(hit);
            }
        }
    }
    public void HitConnected(Collision2D hit) { }

    private void runDamageCalcuation(Collision2D hit)
    {
        HitBox gotHitBox = hit.gameObject.GetComponent<HitBox>();
        Hp -= gotHitBox.getDamage();
        if (Hp < 0)
            Hp = 0;
        Hitstun = 0;
        invulnerable = invincibilityFrames;
        if (Hp == 0)
        {
            hitBox2.setEnable(false);
            hurtBox.off();
            AttackRecovery();
            AttackOver();
            state = 0;
            hold = 80;
            destroyBlock();
        }
    }

    private bool Grounded()
    {
        float HalfWidth = ((transform.lossyScale.x * box.size.x) / 2.0f);
        return GroundRaycastCheck(0) || GroundRaycastCheck(HalfWidth) || GroundRaycastCheck(-HalfWidth);
    }
    private bool GroundRaycastCheck(float offset)
    {
        float HalfHeight = (transform.lossyScale.y * box.size.y) / 2.0f;
        RaycastHit2D[] hit = Physics2D.RaycastAll(new Vector2(transform.position.x + offset, transform.position.y - HalfHeight + .1f), Vector2.down, .15f);
        int size = hit.Length;
        for (int i = 0; i < size; i++)
        {
            if (hit[i].transform.tag.Equals("Ground"))
            {
                return true;
            }
        }
        return false;
    }
    private void checkAttack()
    {
    }
    private void runHitstun()
    {
        animator.SetBool("Dead", false);
        if (Hitstun == 1)
        {
            if (Hp <= 0)
            {
                Load();
            }
        }
        Hitstun--;
    }
    private void runAttack()
    {
        runAttackStates();
    }
    private void runInvulnerable()
    {
        if (invulnerable != 0)
        {
            invulnerable--;
            if (invulnerable == 0)
            {
                spriterenderer.color = new Color(255, 255, 255);
                yellow = false;
            }
            if (invulnerable % 6 == 1)
            {
                if(yellow)
                    spriterenderer.color = new Color(255, 255, 255);
                else
                    spriterenderer.color = new Color(255, 255, 0);
                yellow = !yellow;
            }
        }
    }
    public void attackSetUp()
    {
        if (AttackType == 0)
        {
            int randPos = GameManager.instance.random.Next(0, 2);
            switch (randPos)
            {
                case 0:
                    MoveLeft();
                    break;
                case 1:
                    MoveRight();
                    break;
            }
        }
        else if (AttackType == 1)
        {
            switch (attackPattern[attackIndex])
            {
                case 0:
                    MoveLeft();
                    break;
                case 1:
                    MoveMid();
                    int num = GameManager.instance.random.Next(0, 2);
                    if (num == 0)
                    {
                        if (flip != -1)
                            moveable.flip(transform);
                        flip = -1;
                    }
                    else
                    {
                        if (flip != 1)
                            moveable.flip(transform);
                        flip = 1;
                    }
                    break;
                case 2:
                    MoveRight();
                    break;
            }
        }
        else if(AttackType == 2)
        {
            switch (attackPattern[attackIndex])
            {
                case 0:
                    MoveLeft();
                    break;
                case 1:
                    MoveMid();
                    if (flip != -1)
                        moveable.flip(transform);
                    flip = -1;
                    break;
                case 2:
                    MoveMid();
                    if (flip != 1)
                        moveable.flip(transform);
                    flip = 1;
                    break;
                case 3:
                    MoveRight();
                    break;
            }
        }
    }
    public void SetUp()
    {
        int randPos = GameManager.instance.random.Next(0, 4);
        switch (randPos)
        {
            case 0:
                MoveLeft();
                break;
            case 1:
                MoveMid();
                if (flip != -1)
                    moveable.flip(transform);
                flip = -1;
                break;
            case 2:
                MoveMid();
                if (flip != 1)
                    moveable.flip(transform);
                flip = 1;
                break;
            case 3:
                MoveRight();
                break;
        }
    }
    public void Transform()
    {
        Trans();
        switch(AttackType)
        {
            case -1:
                setNormal();
                break;
            case 0:
                setBombAwl();
                break;
            case 1:
                setArrowAwl();
                break;
            case 2:
                setMeleeAwl();
                break;
        }
    }
    public void Trans()
    {
        transAnimator.SetBool("Run", true);
        transSpriteRender.enabled = true;
    }
    public void Disappear()
    {
        transform.position = Vector3.zero;
    }
    public void AttackActive()
    {
        if (AttackType == 0)
        {
            for (int i = -2; i < 8; i++)
            {
                if (attackPattern[attackIndex] == 0 && (i+8) % 2 == 0)
                    makeBomb((.8f * i));
                else if (attackPattern[attackIndex] == 1 && (i+8) % 2 == 1)
                    makeBomb((.8f * i));
            }
            attackIndex++;
        }
        else if (AttackType == 1)
        {
            if (Pos!=0)
            {
                for (int i = -3; i < 49; i++)
                {
                    makeArrow((.2f * i));
                }
            }
            else
            {
                for (int i = -54; i < 55; i++)
                {
                    makeArrowDown((.2f * i));
                }
            }
        }
        else
        {
            hitBox.setEnable(true);
        }
    }
    private void makeBomb(float Xoff)
    {
        GameObject bomb = Instantiate(BOMB, new Vector3(transform.position.x + (bombXOffset * flip), transform.position.y + bombYOffset, transform.position.z), Quaternion.identity) as GameObject;
        Bomb TempBomb = bomb.GetComponent<Bomb>();
        TempBomb.start();
        TempBomb.move((bombStartX - Xoff) * flip, bombStartY);
    }
    private void makeArrow(float Yoff)
    {
        GameObject arrow = Instantiate(ARROW, new Vector3(transform.position.x + (bombXOffset * flip), transform.position.y + Yoff, transform.position.z), Quaternion.identity) as GameObject;
        Arrow TempArrow = arrow.GetComponent<Arrow>();
        TempArrow.start();
        TempArrow.Flip(flip);
        TempArrow.setDamage(5);
        TempArrow.setSpeed(20);
    }
    private void makeArrowDown(float Xoff)
    {
        GameObject arrow = Instantiate(ARROWDOWN, new Vector3(transform.position.x + (Xoff * flip), transform.position.y+9f, transform.position.z), ARROWDOWN.transform.localRotation) as GameObject;
        Arrow TempArrow = arrow.GetComponent<Arrow>();
        TempArrow.start();
        TempArrow.setDamage(5);
        TempArrow.setSpeed(20);
    }
    private void makeBlock()
    {
        int num = GameManager.instance.random.Next(0,3);
        switch(num)
        {
            case 0:
                BLOCKPattern = Instantiate(BLOCKPattern1, BLOCKPattern1.transform.position, Quaternion.identity) as GameObject;
                break;
            case 1:
                BLOCKPattern = Instantiate(BLOCKPattern2, BLOCKPattern1.transform.position, Quaternion.identity) as GameObject;
                break;
            case 2:
                BLOCKPattern = Instantiate(BLOCKPattern3, BLOCKPattern1.transform.position, Quaternion.identity) as GameObject;
                break;
        }
        madeBlocks = true;
    }
    public void destroyBlock()
    {
        if(madeBlocks)
            GameObject.Destroy(BLOCKPattern.gameObject);
        madeBlocks = false;
    }
    private void MoveRight()
    {
        transform.position = Right;
        Pos = 1;
        if(flip!=-1)
            moveable.flip(transform);
        flip = -1;
    }
    private void MoveLeft()
    {
        transform.position = Left;
        Pos = -1;
        if(flip!=1)
            moveable.flip(transform);
        flip = 1;
    }
    private void MoveMid()
    {
        transform.position = Mid;
        Pos = 0;
    }
    public void AttackOver()
    {
        animator.SetBool("Attack", false);
        hold = 0;
    }
    private void setBombAwl()
    {
        Vector2 offset = new Vector2(0, 0.04f);
        Vector2 size = new Vector2(0.4f, 1.1f);
        setBoxes(offset, size);
        animator.SetBool("Bomb", true);
    }
    private void setArrowAwl()
    {
        Vector2 offset = new Vector2(0, 0.04f);
        Vector2 size = new Vector2(0.45f, 1.4f);
        setBoxes(offset, size);
        animator.SetBool("Arrow", true);
    }
    private void setMeleeAwl()
    {
        Vector2 offset = new Vector2(0.06f, -0.02f);
        Vector2 size = new Vector2(0.8f, 1.95f);
        setBoxes(offset, size);
        animator.SetBool("Melee", true);
    }
    private void setNormal()
    {
        Vector2 offset = new Vector2(0, 0.04f);
        Vector2 size = new Vector2(0.45f ,1.4f);
        setBoxes(offset, size);
    }
    private void setBoxes(Vector2 offset,Vector2 size)
    {
        box.offset = offset;
        hitBoxCollider.offset = offset;
        hurtBoxCollider.offset = offset;
        box.size = size;
        hitBoxCollider.size = size;
        hurtBoxCollider.size = size;
        animator.SetBool("Arrow",false);
        animator.SetBool("Bomb",false);
        animator.SetBool("Melee",false);
    }
    public void AttackRecovery()
    {
        hitBox.setEnable(false);
    }
    public void enableSprite(bool value)
    {
        hurtBox.setEnable(value);
        hitBox2.setEnable(value);
        spriterenderer.enabled = value;
    }
    private void runStates()
    {
        if (hold == 0)
        {
            switch (state)
            {
                case 0:
                    enableSprite(true);
                    state = 1;
                    hold = 50;
                    break;
                case 1:
                    AttackType = GameManager.instance.random.Next(0, 3);
                    Transform();
                    hold = 50;
                    state = 2;
                    break;
                case 2:
                    if (AttackType == 1)
                        makeBlock();
                    Trans();
                    enableSprite(false);
                    hold = 100;
                    state = 3;
                    break;
                case 3:
                    attaking = true;
                    setAttackPattern();
                    state = 4;
                    break;
                case 4:
                    enableSprite(true);
                    state = 5;
                    hold = 50;
                    break;
                case 5:
                    AttackType = -1;
                    Transform();
                    hold = 50;
                    state = 6;
                    break;
                case 6:
                    Trans();
                    enableSprite(false);
                    hold = 100;
                    state = 0;
                    break;
            }
        }
        else if (hold == 10)
        {
            switch (state)
            {
                case 0:
                    SetUp();
                    Trans();
                    enableSprite(false);
                    break;
                case 4:
                    SetUp();
                    Trans();
                    enableSprite(false);
                    break;
            }
            hold--;
        }
        else if (hold == 50)
        {
            switch (state)
            {
                case 0:
                    Disappear();
                    break;
                case 3:
                    Disappear();
                    break;
            }
            hold--;
        }
        else
            hold--;
    }
    private void RunDeathStates()
    {
        if (hold == 0)
        {
            switch (state)
            {
                case 0:
                    hold = 80;
                    state = 1;
                    break;
                case 1:
                    Trans();
                    spriterenderer.enabled = false;
                    hold = 80;
                    break;
                case 2:
                    spriterenderer.enabled = true;
                    state = 3;
                    hold = 80;
                    break;
                case 3:
                    AttackType = -1;
                    Transform();
                    Hitstun = 150;
                    animator.SetBool("Dead", true);
                    break;
            }
        }
        else if (hold == 10)
        {
            switch (state)
            {
                case 2:
                    MoveMid();
                    if (flip != -1)
                        moveable.flip(transform);
                    flip = -1;
                    Trans();
                    spriterenderer.enabled = false;
                    break;
            }
            hold--;
        }
        else if (hold == 40)
        {
            switch (state)
            {
                case 1:
                    Disappear();
                    state = 2;
                    break;
            }
            hold--;
        }
        else
            hold--;
    }
    private void setAttackPattern()
    {
        if (AttackType == 0)
        {
            patternLength = 2 + (((int)(300 - Hp)) / 50);
            attackPattern = new int[patternLength];
            int first = GameManager.instance.random.Next(0,2);
            if(first == 0)
            {
                attackPattern[0] = 0;
                attackPattern[1] = 1;
            }
            else
            {
                attackPattern[0] = 1;
                attackPattern[1] = 0;
            }
            for(int i = 0; i<patternLength-2;i++)
            {
                int num = GameManager.instance.random.Next(0, 2);
                attackPattern[2+i] = num;
            }
        }
        else if (AttackType == 1)
        {
            patternLength = 3 + (((int)(300 - Hp)) / 50);
            attackPattern = new int[patternLength];
            int num = GameManager.instance.random.Next(0, 3);
            attackPattern[0] = num;
            for (int i = 1; i < patternLength; i++)
            {
                num = GameManager.instance.random.Next(0, 2);
                switch (attackPattern[i-1])
                {
                    case 0:
                        attackPattern[i] = num==0?1:2;
                        break;
                    case 1:
                        attackPattern[i] = num == 0 ? 0 : 2;
                        break;
                    case 2:
                        attackPattern[i] = num == 0 ? 0 : 1;
                        break;
                }
            }
        }
        else if(AttackType == 2)
        {
            patternLength = 4 + (((int)(300 - Hp)) / 50);
            attackPattern = new int[patternLength];
            for (int i = 0; i < patternLength; i++)
            {
                int num = GameManager.instance.random.Next(0, 2);
                if (i % 2 == 0)
                {
                    if(num==0)
                        attackPattern[i] = 0;
                    else
                        attackPattern[i] = 3;
                }
                else
                {
                    if (num == 0)
                        attackPattern[i] = 1;
                    else
                        attackPattern[i] = 2;
                }
            }
        }
        attackIndex = 0;
        attackState = 0;
        hold = 100;
    }
    private void runAttackStates()
    {
        switch (AttackType)
        {
            case 0:
                bombPattern();
                break;
            case 1:
                arrowPattern();
                break;
            case 2:
                meleePattern();
                break;
        }
    }
    private void bombPattern()
    {
        if (hold == 0)
        {
            switch (attackState)
            {
                case 0:
                    enableSprite(true);
                    attackState = 1;
                    hold = 0;
                    break;
                case 1:
                    if (attackIndex == patternLength)
                    {
                        attackState = 2;
                        hold = 50;
                    }
                    else
                    {
                        animator.SetBool("Attack", true);
                        hold = 10000;
                    }
                    break;
                case 2:
                    Trans();
                    enableSprite(false);
                    hold = 100;
                    attackState = 3;
                    break;
            }
        }
        else if (hold == 10)
        {
            switch (attackState)
            {
                case 0:
                    attackSetUp();
                    Trans();
                    enableSprite(false);
                    break;
            }
            hold--;
        }
        else if (hold == 50)
        {
            switch (attackState)
            {
                case 3:
                    Disappear();
                    attaking = false;
                    animator.SetBool("Attack", false);
                    break;
            }
            hold--;
        }
        else
            hold--;
    }
    private void arrowPattern()
    {
        if (hold == 0)
        {
            switch (attackState)
            {
                case 0:
                    enableSprite(true);
                    attackState = 1;
                    hold = 0;
                    break;
                case 1:
                    if (attackIndex == patternLength-1)
                    {
                        animator.SetBool("Attack", true);
                        attackState = 2;
                        hold = 10000;
                    }
                    else
                    {
                        animator.SetBool("Attack", true);
                        attackState = 4;
                        hold = 10000;
                        attackIndex++;
                    }
                    break;
                case 2:
                    Trans();
                    enableSprite(false);
                    hold = 100;
                    attackState = 3;
                    break;
                case 4:
                    Trans();
                    enableSprite(false);
                    hold = 100;
                    attackState = 0;
                    break;
            }
        }
        else if (hold == 10)
        {
            switch (attackState)
            {
                case 0:
                    attackSetUp();
                    Trans();
                    enableSprite(false);
                    break;
            }
            hold--;
        }
        else if (hold == 50)
        {
            switch (attackState)
            {
                case 0:
                    Disappear();
                    break;
                case 3:
                    Disappear();
                    attaking = false;
                    animator.SetBool("Attack", false);
                    destroyBlock();
                    break;
            }
            hold--;
        }
        else
            hold--;
    }
    private void meleePattern()
    {
        if (hold == 0)
        {
            switch (attackState)
            {
                case 0:
                    enableSprite(true);
                    attackState = 1;
                    hold = 0;
                    break;
                case 1:
                    if (attackIndex == patternLength-1)
                    {
                        animator.SetBool("Attack", true);
                        attackState = 2;
                        hold = 10000;
                    }
                    else
                    {
                        animator.SetBool("Attack", true);
                        attackState = 4;
                        hold = 10000;
                        attackIndex++;
                    }
                    break;
                case 2:
                    Trans();
                    enableSprite(false);
                    hold = 100;
                    attackState = 3;
                    break;
                case 4:
                    Trans();
                    enableSprite(false);
                    hold = 100;
                    attackState = 0;
                    break;
            }
        }
        else if (hold == 10)
        {
            switch (attackState)
            {
                case 0:
                    attackSetUp();
                    Trans();
                    enableSprite(false);
                    break;
            }
            hold--;
        }
        else if (hold == 50)
        {
            switch (attackState)
            {
                case 0:
                    Disappear();
                    break;
                case 3:
                    Disappear();
                    attaking = false;
                    animator.SetBool("Attack", false);
                    break;
            }
            hold--;
        }
        else
            hold--;
    }
    private void Load()
    {
        GameManager.instance.bossDead = true;
        GameObject player = GameObject.Find("Player");
        GameObject cam = GameObject.Find("Main Camera");
        GameManager.instance.PlayerFlip = player.GetComponent<Player>().getFlip();
        GameManager.instance.PlayerHp = player.GetComponent<Player>().getHP();
        GameManager.instance.newXpos = player.transform.position.x;
        GameManager.instance.newYpos = player.transform.position.y;
        GameManager.instance.newCamXpos = cam.transform.position.x;
        GameManager.instance.newCamYpos = cam.transform.position.y;
        GameManager.instance.Load("BossDeath", "");
    }
}
