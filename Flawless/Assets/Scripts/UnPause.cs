﻿using UnityEngine;
using System.Collections;

public class UnPause : MonoBehaviour 
{

	void Start () 
    {
        Time.timeScale = 1;
        GameManager.instance.reset();
	}
	
}
