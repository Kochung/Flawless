﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour, HitAble
{
    private Rigidbody2D body;
    private Animator animator;
    private SpriteRenderer spriterenderer;
    private Moveable moveable;
    private BoxCollider2D box;
    private HurtBox hurtBox;
    private HitBox hitBox;
    private HitBox hitBox2;
    private HitBox hitBox3;
    private LifeBar lifeBar;
    public GameObject arrow;

    public AudioClip Attack1Sound;
    public AudioClip Attack2Sound;
    public AudioClip Attack3Sound;
    public AudioClip ArrowSound;
    public AudioClip Jump1Sound;
    public AudioClip Jump2Sound;

    private bool jumpCancel;
    private int flip;
    private float HP;
    private int maxHP;
    private bool attaking;
    private int Hitstun;
    private int invulnerable;
    private bool jumped;
    private bool fired;
    private bool firedArrow;
    private bool fixGround;
    private int Horizontal;
    private bool oneFrameStop;
    private bool recovery;
    private bool attack2;
    private float[] AttackValues = { 3,3.2f,3.5f,4,4.5f,5,6,8,10,15,20,30};
    private int JumpAmount;

    public int invincibilityFrames;
    public float moveSpeed;
    public float jumpSpeed;
    public float maxDownVelocity;
    public float minJumpSpeed;
    public float maxCancelSpeed;
    public float cancelSetSpeed;

    void Start ()
    {
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriterenderer = GetComponent<SpriteRenderer>();
        box = GetComponent<BoxCollider2D>();
        moveable = new Moveable(body, maxDownVelocity);
        lifeBar = GameObject.Find("LifeBar").GetComponent<LifeBar>();

        if (!GameManager.instance.first)
            transform.position = new Vector2(GameManager.instance.newXpos, GameManager.instance.newYpos);
		HP = GameManager.instance.PlayerHp;
        maxHP = GameManager.instance.PlayerMaxHp;
        flip = GameManager.instance.PlayerFlip;
        if (flip == -1)
            moveable.flip(transform);

        hurtBox = transform.GetChild(0).GetComponent<HurtBox>();
        hitBox = transform.GetChild(1).GetComponent<HitBox>();
        hitBox2 = transform.GetChild(2).GetComponent<HitBox>();
        hitBox3 = transform.GetChild(3).GetComponent<HitBox>();
        hitBox.setEnable(false);
        hitBox.setDamage(AttackValues[GameManager.instance.GlobalAttackUp]);
        hitBox.setParent(this);

        hitBox2.setEnable(false);
        hitBox2.setDamage(AttackValues[GameManager.instance.GlobalAttackUp]);
        hitBox2.setParent(this);

        hitBox3.setEnable(false);
        hitBox3.setDamage(AttackValues[GameManager.instance.GlobalAttackUp]);
        hitBox3.setParent(this);

        hurtBox.setParent(this);

    }
    void Update()
    {
        if (Time.timeScale == 0) return; 
        if (GameSettings.instance.cutScene)
        {
            Horizontal = GameSettings.instance.cutSceneRunning;
            fired = false;
            firedArrow = false;
            jumped = false;
            fixGround = Grounded() && body.velocity.y <= 0.05f;
            return;
        }
        if(oneFrameStop)
        {
            oneFrameStop = false;
            return;
        }
        if(Input.GetButtonDown("Jump") && Input.GetAxisRaw("Down") ==0)
            jumped = true;
        if (Input.GetButtonUp("Jump"))
            jumped = false;
        if (Input.GetButtonDown("Attack"))
            fired = true;
        if (Input.GetButtonUp("Attack"))
            fired = false;
        if (Input.GetButtonDown("Attack 2"))
            firedArrow = true;
        if (Input.GetButtonUp("Attack 2"))
            firedArrow = false;

        Horizontal = 0;
        if (((int)Input.GetAxisRaw("Left"))==1)
            Horizontal--;
        if (((int)Input.GetAxisRaw("Right"))==1)
            Horizontal++;
        fixGround = Grounded() && body.velocity.y <= 0.05f;
    }
    void FixedUpdate()
    {
        if (Hitstun != 0)
        {
            runHitstun();
        }
        else if (attaking)
        {
            runAttack();
            if (recovery)
            {
                if(!Attack())
                {
                    if (!fixGround)
                        MoveHorizontal();
                    JumpCheck();
                    Jumped();
                }
                else
                    JumpCheck();
            }
            else
            {
                if (!fixGround)
                    MoveHorizontal();
                if (attack2)
                    moveable.addX(5*flip);
                JumpCheck();
                Jumped();
            }
        }
        else
        {
            if (!Attack())
            {
                MoveHorizontal();
                JumpCheck();
                Jumped();
            }
            else
                JumpCheck();
        }
        runInvulnerable();
        moveable.CheckY();
    }

    public void gotHit(Collision2D hit)
    {
        if (hit.gameObject.tag.Equals("Enemy") || hit.gameObject.tag.Equals("HitAll"))
        {
            if (Hitstun == 0 && invulnerable == 0)
            {
                animator.SetBool("Hit", true);
                runDamageCalcuation(hit);
                AttackRecovery(0);
                AttackOver(0);
                if (HP <= 0)
                {
                    GameObject.Find("GameOver").GetComponent<SpriteRenderer>().enabled = true;
                    lifeBar.clear();
                }
            }
        }
    }
    public void HitConnected(Collision2D hit){}

    private void runDamageCalcuation(Collision2D hit)
    {
        HitBox gotHitBox = hit.gameObject.GetComponent<HitBox>();
        HP -= gotHitBox.getDamage();
        if (HP < 0)
            HP = 0;
        lifeBar.updateHp((int)HP);
        int direction = 1;
        if (hit.gameObject.transform.position.x - transform.position.x < 0)
            direction = -1;
        moveable.moveY(gotHitBox.knockbackY);
        moveable.setForceX(gotHitBox.knockbackX * direction);
        Hitstun = gotHitBox.hitStun;
        if (HP == 0)
        {
            Hitstun = 150;
            animator.SetBool("Dead", true);
            hurtBox.off();
        }
        animator.SetBool("Hitstun", true);
    }

    private bool Grounded()
    {
        float HalfWidth = ((transform.lossyScale.x*box.size.x)/2.0f);
        return GroundRaycastCheck(0)|| GroundRaycastCheck(HalfWidth)|| GroundRaycastCheck(-HalfWidth);
    }
    private bool GroundRaycastCheck(float offset)
    {
        float HalfHeight = (transform.lossyScale.y * box.size.y) / 2.0f;
        RaycastHit2D[] hit = Physics2D.RaycastAll(new Vector2(transform.position.x+offset, transform.position.y - HalfHeight + .1f), Vector2.down, .15f);
        int size = hit.Length;
        for (int i = 0; i < size; i++)
        {
            if (hit[i].transform.tag.Equals("Ground"))
            { 
                return true;
            }
        }
        return false;
    }

    private bool Attack()
    {
        if (fired && Horizontal == flip && GameManager.instance.Attack2)
        {
            AttackOver(0);
            animator.SetBool("Attack2", true);
            attaking = true;
            fired = false;
            return true;
        }
        else if (fired && Input.GetAxisRaw("Down") == 1 && GameManager.instance.Attack3)
        {
            AttackOver(0);
            animator.SetBool("Attack3", true);
            attaking = true;
            fired = false;
            return true;
        }
        else if (fired)
        {
            AttackOver(0);
            animator.SetBool("Attack", true);
            attaking = true;
            fired = false;
            return true;
        }
        else if (firedArrow && GameManager.instance.Bow)
        {
            animator.SetBool("AttackArrow", true);
            firedArrow = false;
            return true;
        }
        return false;
    }
    private void MoveHorizontal()
    {
        if (Horizontal != 0)
        {
            moveable.moveX(moveSpeed * Horizontal);
            animator.SetBool("Run", true);
            if (flip != Horizontal)
            {
                flip = (int)Horizontal;
                moveable.flip(transform);
            }
        }
        else
        {
            moveable.moveX(0);
            animator.SetBool("Run", false);
        }
    }
    private void Jumped()
    {
        if (jumped && fixGround)
        {
            moveable.moveY(jumpSpeed);
            jumpCancel = true;
            jumped = false;
            animator.SetBool("Jump", true);
            animator.SetBool("Peak", false);
            animator.SetBool("Fall", false);
            SoundManager.instance.PlaySingle(Jump1Sound);
        }
        if (Input.GetAxisRaw("Jump") == 0 && jumpCancel && body.velocity.y > maxCancelSpeed && body.velocity.y < minJumpSpeed)
        {
            moveable.moveY(cancelSetSpeed);
            jumpCancel = false;
        }
        if(jumped && JumpAmount < GameManager.instance.DoubleJump)
        {
            moveable.moveY(jumpSpeed);
            jumpCancel = true;
            jumped = false;
            animator.SetBool("Jump", true);
            animator.SetBool("Peak", false);
            animator.SetBool("Fall", false);
            SoundManager.instance.PlaySingle(Jump2Sound);
            JumpAmount++;
        }
    }
    private void JumpCheck()
    {
        animator.SetBool("Jump", false);
        if (body.velocity.y < 1)
            animator.SetBool("Peak", true);
        if (body.velocity.y < -1 && !Grounded())
            animator.SetBool("Fall", true);
        if (fixGround)
        {
            animator.SetBool("Peak", false);
            animator.SetBool("Fall", false);
            animator.SetBool("Land", true);
            JumpAmount = 0;
        }
        else
            animator.SetBool("Land", false);
        if(body.velocity.y<0)
            body.gravityScale = 3.0f;
        else
            body.gravityScale = 1.0f;
    }
    private void runAttack()
    {
        moveable.moveX(0);
    }
    private void runHitstun()
    {
        animator.SetBool("Hit", false);
        if (HP == 0)
            moveable.moveX(0);
        if (fixGround)
            moveable.moveX(0);
        else
            moveable.ForceX();
        if (Hitstun == 1)
        {
            if (HP <= 0)
            {
                GameManager.instance.reset();
                GameManager.instance.Load("MainMenu","main");
            }
            else
            {
                animator.SetBool("Hitstun", false);
                invulnerable = invincibilityFrames;
            }
        }
        Hitstun--;
    }
    private void runInvulnerable()
    {
        if (invulnerable != 0)
        {
            invulnerable--;
            if (invulnerable % 3 == 1)
                spriterenderer.enabled = false;
            else
                spriterenderer.enabled = true;
        }
    }

    public int getFlip() { return flip; }
    public float getHP() { return HP; }
    public void attackup()
    {
        hitBox.setDamage(AttackValues[GameManager.instance.GlobalAttackUp]);
    }
    public void MaxHpup()
    {
        maxHP = GameManager.instance.PlayerMaxHp;
        lifeBar.AddMaxLife(maxHP);
    }
    public void Heal(int amount)
    {
        HP += amount;
        if (HP > maxHP)
            HP = maxHP;
        lifeBar.updateHp((int)HP);
    }
    public void AttackActive(int box)
    {
        switch(box)
        {
            case 0:
                hitBox.setEnable(true);
                hitBox2.setEnable(true);
                hitBox3.setEnable(true);
                break;
            case 1:
                hitBox.setEnable(true);
                hitBox2.setEnable(false);
                hitBox3.setEnable(false);
                break;
            case 2:
                hitBox2.setEnable(true);
                hitBox.setEnable(false);
                hitBox3.setEnable(false);
                attack2 = true;
                break;
            case 3:
                hitBox3.setEnable(true);
                hitBox.setEnable(false);
                hitBox2.setEnable(false);
                break;
        }
    }
    public void AttackRecovery(int box)
    {
        switch (box)
        {
            case 0:
                hitBox.setEnable(false);
                hitBox2.setEnable(false);
                hitBox3.setEnable(false);
                break;
            case 1:
                hitBox.setEnable(false);
                break;
            case 2:
                hitBox2.setEnable(false);
                break;
            case 3:
                hitBox3.setEnable(false);
                break;
        }
        recovery = true;
    }
    public void AttackOver(int box)
    {
        attaking = false;
        switch (box)
        {
            case 0:
                animator.SetBool("Attack", false);
                animator.SetBool("Attack2", false);
                animator.SetBool("Attack3", false);
                animator.SetBool("AttackArrow", false);
                break;
            case 1:
                animator.SetBool("Attack", false);
                break;
            case 2:
                animator.SetBool("Attack2", false);
                break;
            case 3:
                animator.SetBool("Attack3", false);
                break;
            case 4:
                animator.SetBool("AttackArrow", false);
                break;
        }

        attack2 = false;
        recovery = false;
    }
    public void Unpause()
    {
        oneFrameStop = true;
    }
    public void SpawnArrow()
    {
        Arrow tempArrow = (Instantiate(arrow, new Vector3(transform.position.x+(.5f*flip), transform.position.y+.25f, transform.position.z), Quaternion.identity) as GameObject).GetComponent<Arrow>();
        tempArrow.start();
        tempArrow.Flip(flip);
        tempArrow.setDamage(AttackValues[GameManager.instance.GlobalAttackUp]/2);
    }
    public void playAttack(int number)
    {
        switch (number)
        {
            case 1:
                SoundManager.instance.PlaySingle(Attack1Sound);
                break;
            case 2:
                SoundManager.instance.PlaySingle(Attack2Sound);
                break;
            case 3:
                SoundManager.instance.PlaySingle(Attack3Sound);
                break;
            case 4:
                SoundManager.instance.PlaySingle(ArrowSound);
                break;
        }
    }
}
