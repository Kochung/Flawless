﻿using UnityEngine;
using System.Collections;

public class LifeBar : MonoBehaviour {

    public GameObject Left;
    public GameObject Right;
    public GameObject Mid;
    public GameObject Tank;
    private GameObject [] Mids;
    private GameObject[] Tanks;
    private int currentHp;
    private int MaxHp;
    private int smallMaxHp;
    private int barSize;

    public float offsetX;
    public float offsetY;
    public float spaceSize;
    public float tankSpace;
    public float midAdjustment;
    public float tankAdjustment;
    public float rightAdjustment;
    // Use this for initialization
    void Start ()
    {
        offsetX += transform.position.x;
        offsetY += transform.position.y;
        currentHp = (int)GameManager.instance.PlayerHp;
        MaxHp = GameManager.instance.PlayerMaxHp;
        smallMaxHp = 10;
        barSize = 0;
        Mids = new GameObject[25];
        Tanks = new GameObject[3];
        for (int i = 0; i < 25; i++)
        {
            Mids[i] = Instantiate(Mid, new Vector3(offsetX+(midAdjustment + (spaceSize * i)), offsetY, 0), Quaternion.identity) as GameObject;
            Mids[i].transform.parent = transform;
        }
        for(int i=0;i<3;i++)
        {
            Tanks[i] = Instantiate(Tank, new Vector3(offsetX + (tankAdjustment + (spaceSize * (-1)+(tankSpace*i))), offsetY, 0), Quaternion.identity) as GameObject;
            Tanks[i].transform.parent = transform;
            enableTank(i,false);
        }
        Left = Instantiate(Left, new Vector3(offsetX, offsetY, 0), Quaternion.identity) as GameObject;
        Right = Instantiate(Right, new Vector3(offsetX + (rightAdjustment + (spaceSize * -1)), offsetY, 0), Quaternion.identity) as GameObject;
        Left.transform.parent = transform;
        Right.transform.parent = transform;
        AddMaxLife(MaxHp);
        updateHp(currentHp);
        GameManager.instance.ShowBossKey();
        GameManager.instance.updateKeys(0);
    }
    public void AddMaxLife(int newMaxHp)
    {
        int amount = ((newMaxHp-1) / 25)-1;
        for (int i = 0; i <= amount; i++)
            enableTank(i,true);
        MaxHp = newMaxHp;
        smallMaxHp = MaxHp - (amount+1) * 25;
    }
    public void updateHp(int newHp)
    {
        int newBarSize = smallMaxHp;
        if(MaxHp > 25)
        {
            int amount = ((newHp-1) / 25);
            setTank(0, amount, true);
            setTank(amount, 3, false);
            newHp -= 25*amount;
            if (smallMaxHp!=25 && amount != ((MaxHp - 1) / 25))
                newBarSize = 25;
        }
        if(newBarSize != barSize)
        {
            moveRight(barSize-newBarSize);
            barSize = newBarSize;
            enablePip(0,barSize,true);
            enablePip(barSize,25, false);
            currentHp = barSize;
        }
        if (newHp < currentHp)
        {
            for (int i = newHp; i < currentHp; i++) 
                setPip(i,false);
        }
        else
        {
            for (int i = currentHp; i < newHp; i++)
                setPip(i, true);
        }
        currentHp = newHp;
    }
    public void clear()
    {
        Left.GetComponent<SpriteRenderer>().enabled = false;
        Right.GetComponent<SpriteRenderer>().enabled = false;
        for (int i = 0; i < 25; i++)
        {
            enablePip(i,false);
        }
        for (int i = 0; i < 3; i++)
        {
            enableTank(i, false);
        }
    }
    public void moveRight(int amount)
    {
        Right.transform.position = new Vector3(Right.transform.position.x - (spaceSize * amount ), Right.transform.position.y, 0);
        for (int i = 0; i < 3; i++)
        {
            Tanks[i].transform.position = new Vector3(Tanks[i].transform.position.x - (spaceSize * amount), Tanks[i].transform.position.y, 0);
        }
    }
    private void enablePip(int index ,bool enable)
    {
        Mids[index].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = enable;
        Mids[index].GetComponent<SpriteRenderer>().enabled = enable;
    }
    private void enableTank(int index, bool enable)
    {
        Tanks[index].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = enable;
        Tanks[index].GetComponent<SpriteRenderer>().enabled = enable;
    }
    public void enablePip(int start, int end, bool enable)
    {
        for (int i = start; i < end; i++)
            enablePip(i, enable);
    }
    public void enableTank(int start, int end, bool enable)
    {
        for (int i = start; i < end; i++)
            enableTank(i, enable);
    }
    private void setPip(int index, bool enable)
    {
        if (Mids[index].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled)
            Mids[index].GetComponent<SpriteRenderer>().enabled = enable;
    }
    private void setTank(int index, bool enable)
    {
        if(Tanks[index].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled)
            Tanks[index].GetComponent<SpriteRenderer>().enabled = enable;
    }
    public void setPip(int start, int end, bool enable)
    {
        for (int i = start; i < end; i++)
            setPip(i, enable);
    }
    public void setTank(int start, int end, bool enable)
    {
        for (int i = start; i < end; i++)
            setTank(i, enable);
    }
}
