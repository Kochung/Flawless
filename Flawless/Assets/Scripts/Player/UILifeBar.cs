﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UILifeBar : MonoBehaviour
{

    public GameObject Left;
    public GameObject Right;
    public GameObject Mid;
    public GameObject Tank;
    private GameObject[] Mids;
    private GameObject[] Tanks;
    private int currentHp;
    private int MaxHp;
    private int smallMaxHp;
    private int barSize;

    public float offsetX;
    public float offsetY;
    public float spaceSize;
    public float tankSpace;
    public float midAdjustment;
    public float tankAdjustment;
    public float rightAdjustment;
    // Use this for initialization
    public void open(int mhp,int chp)
    {
        float scaleAdjustment = Screen.width / 800f; //800f is the width of the SaveScrollView
        spaceSize *= scaleAdjustment;
        tankSpace *= scaleAdjustment;
        midAdjustment *= scaleAdjustment;
        tankAdjustment *= scaleAdjustment;
        rightAdjustment *= scaleAdjustment;
        Vector3 scale = Vector3.one * scaleAdjustment;

        offsetX += transform.position.x;
        offsetY += transform.position.y;
        currentHp = chp;
        MaxHp = mhp;
        smallMaxHp = 10;
        barSize = 0;
        Mids = new GameObject[25];
        Tanks = new GameObject[3];
        for (int i = 0; i < 25; i++)
        {
            Mids[i] = Instantiate(Mid, new Vector3(offsetX + (midAdjustment + (spaceSize * i)), offsetY, 0), Quaternion.identity) as GameObject;
            Mids[i].transform.localScale = scale;
            Mids[i].transform.SetParent(transform);
        }
        for (int i = 0; i < 3; i++)
        {
            Tanks[i] = Instantiate(Tank, new Vector3(offsetX + (tankAdjustment + (spaceSize * (-1) + (tankSpace * i))), offsetY, 0), Quaternion.identity) as GameObject;
            Tanks[i].transform.localScale = scale;
            Tanks[i].transform.SetParent(transform);
            enableTank(i, false);
        }
        Left = Instantiate(Left, new Vector3(offsetX, offsetY, 0), Quaternion.identity) as GameObject;
        Right = Instantiate(Right, new Vector3(offsetX + (rightAdjustment + (spaceSize * -1)), offsetY, 0), Quaternion.identity) as GameObject;
        Left.transform.localScale = scale;
        Right.transform.localScale = scale;
        Left.transform.SetParent(transform);
        Right.transform.SetParent(transform);
        AddMaxLife(MaxHp);
        updateHp(currentHp);
    }
    public void AddMaxLife(int newMaxHp)
    {
        int amount = ((newMaxHp - 1) / 25) - 1;
        for (int i = 0; i <= amount; i++)
            enableTank(i, true);
        MaxHp = newMaxHp;
        smallMaxHp = MaxHp - (amount + 1) * 25;
    }
    public void updateHp(int newHp)
    {
        int newBarSize = smallMaxHp;
        if (MaxHp > 25)
        {
            int amount = ((newHp - 1) / 25);
            setTank(0, amount, true);
            setTank(amount, 3, false);
            newHp -= 25 * amount;
            if (smallMaxHp != 25 && amount != ((MaxHp - 1) / 25))
                newBarSize = 25;
        }
        if (newBarSize != barSize)
        {
            moveRight(barSize - newBarSize);
            barSize = newBarSize;
            enablePip(0, barSize, true);
            enablePip(barSize, 25, false);
            currentHp = barSize;
        }
        if (newHp < currentHp)
        {
            for (int i = newHp; i < currentHp; i++)
                setPip(i, false);
        }
        else
        {
            for (int i = currentHp; i < newHp; i++)
                setPip(i, true);
        }
        currentHp = newHp;
    }
    public void clear()
    {
        Left.GetComponent<RawImage>().enabled = false;
        Right.GetComponent<RawImage>().enabled = false;
        for (int i = 0; i < 25; i++)
        {
            enablePip(i, false);
        }
        for (int i = 0; i < 3; i++)
        {
            enableTank(i, false);
        }
    }
    public void moveRight(int amount)
    {
        Right.transform.position = new Vector3(Right.transform.position.x - (spaceSize * amount), Right.transform.position.y, 0);
        for (int i = 0; i < 3; i++)
        {
            Tanks[i].transform.position = new Vector3(Tanks[i].transform.position.x - (spaceSize * amount), Tanks[i].transform.position.y, 0);
        }
    }
    private void enablePip(int index, bool enable)
    {
        Mids[index].transform.GetChild(0).GetComponent<RawImage>().enabled = enable;
        Mids[index].GetComponent<RawImage>().enabled = enable;
    }
    private void enableTank(int index, bool enable)
    {
        Tanks[index].transform.GetChild(0).GetComponent<RawImage>().enabled = enable;
        Tanks[index].GetComponent<RawImage>().enabled = enable;
    }
    public void enablePip(int start, int end, bool enable)
    {
        for (int i = start; i < end; i++)
            enablePip(i, enable);
    }
    public void enableTank(int start, int end, bool enable)
    {
        for (int i = start; i < end; i++)
            enableTank(i, enable);
    }
    private void setPip(int index, bool enable)
    {
        if (Mids[index].GetComponent<RawImage>().enabled)
            Mids[index].transform.GetChild(0).GetComponent<RawImage>().enabled = enable;
    }
    private void setTank(int index, bool enable)
    {
        if (Tanks[index].GetComponent<RawImage>().enabled)
            Tanks[index].transform.GetChild(0).GetComponent<RawImage>().enabled = enable;
    }
    public void setPip(int start, int end, bool enable)
    {
        for (int i = start; i < end; i++)
            setPip(i, enable);
    }
    public void setTank(int start, int end, bool enable)
    {
        for (int i = start; i < end; i++)
            setTank(i, enable);
    }
}
