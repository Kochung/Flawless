﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class ChangeMenu : MonoBehaviour
{
    public Button button;
    public string changeSceneName;
    public string changeSongName;

    void Start()
    {
        if (button != null)
            button.onClick.AddListener(LoadScene);
    }

    public void LoadScene()
    {
        if (changeSceneName == "Exit")
        {
            Application.Quit();
            return;
        }
        if(changeSceneName == "Continue")
        {
			fade("LoadContinue");
            return;
        }
        if (button != null && button.name.Equals("OptionsBackButton"))
        {
            GameManager.instance.saveGameSetting();
        }
        else if (changeSceneName == "ConfirmLoad Name")
        {
            changeSceneName = "ConfirmLoad";
            GameManager.instance.fileName = button.transform.GetChild(0).GetComponent<Text>().text;
        }
        else if (button != null && button.name.Equals("Yes"))
        {
            File.Delete("./Saves/" + GameManager.instance.fileName+".jlts");
            GameManager.instance.fileName = "";
        }
        else if (button != null && button.name.Equals("NG+Yes"))
        {
            GameManager.instance.newGamePlusSave();
        }
        else if(button != null && button.name.Equals("LoadGame"))
		{
			fade("LoadSave");
			return;
		}
        GameObject fader = GameObject.FindGameObjectWithTag("Fader");
        if (fader != null)
        {
            fader.BroadcastMessage("Enable");
            fader.BroadcastMessage("FadeOut");
            Invoke("LoadSceneImmidiately", .23f); //a bit faster than fade out time (0.25)
            return;
        }
        else LoadSceneImmidiately();
    }

    void LoadSceneImmidiately()
    {
        Application.LoadLevel(changeSceneName);
        SoundManager.instance.PlayMusic(changeSongName);
	}
	private void LoadContinue()
	{
		GameManager.instance.LoadContinue();
	}
	private void LoadSave ()
	{
		GameManager.instance.LoadSave();
	}
	private void fade(string method)
	{
		GameObject fader = GameObject.FindGameObjectWithTag("Fader");
		if (fader != null)
		{
			fader.BroadcastMessage("Enable");
			fader.BroadcastMessage("FadeOut");
			Invoke(method, .23f);
		}
	}
}