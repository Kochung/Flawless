﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Credits
{
    public string title;
    public string[] names;

    /// <summary>
    /// Credit text for CreditsPanel to display
    /// </summary>
    /// <param name="title">The credit title</param>
    /// <param name="names">Maximum of 4 names only please. If you need more then create another credits.</param>
    public Credits(string title, params string[] names)
    {
        this.title = title;
        this.names = names;
        if (this.names.Length > 4)
            this.names = new string[4] { names[0], names[1], names[2], names[3] };
    }
}
