﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CreditsPanel : MonoBehaviour
{
    public Text title;
    public Text[] lines;

    private Image image;
    private CanvasGroup canvasGroup;

    private List<Credits> credits = new List<Credits>();
    private int cid = 0;

    void Awake()
    {
        image = GetComponent<Image>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void Start()
    {
        credits.Add(new Credits("Team Lead", "James Wang"));
        credits.Add(new Credits("Programmers", "Samuel Huang", "Tri Tran"));
        credits.Add(new Credits("Artist", "Louis Barrios"));
        credits.Add(new Credits("Music", "Mark Sparling", "(marksparling.com)"));
        credits.Add(new Credits("Sound Effects", "StumpyStrust"));
        credits.Add(new Credits("Fonts", "\"bladeline\" by designstation", "\"Ace Futurism\" by NAL"));
        NextCredit();
    }

    public void NextCredit()
    {
        Credits crd = credits[cid];
        title.text = crd.title;
        int i = 0;
        for (; i < crd.names.Length; ++i)
            lines[i].text = crd.names[i];
        for (; i < lines.Length; ++i)
            lines[i].text = "";
        cid = (cid + 1) % credits.Count;
    }

    void FixedUpdate()
    {
        canvasGroup.alpha = image.color.a;
    }
}
