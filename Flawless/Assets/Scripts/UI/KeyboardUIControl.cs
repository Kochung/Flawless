﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class KeyboardUIControl : MonoBehaviour
{
    public Selectable defaultSelectable;

    //[HideInInspector]
    public bool hasSelected = false;

    private EventSystem eventSystem;
    private bool typing = false;

    void Awake()
    {
        eventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();
    }

    void Update()
    {
        if (!typing && (Input.GetButtonDown("Up") || Input.GetButtonDown("Down")|| Input.GetButtonDown("Right") || Input.GetButtonDown("Left")) && !hasSelected && defaultSelectable != null)
        {
            eventSystem.SetSelectedGameObject(null);
            hasSelected = true;
            defaultSelectable.Select();
        }
        if (Mathf.Abs(Input.GetAxis("Mouse X")) > 0f || Mathf.Abs(Input.GetAxis("Mouse Y")) > 0f
            || Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            if (hasSelected)
                eventSystem.SetSelectedGameObject(null);
            hasSelected = false;
        }
    }

    void OnDisable()
    {
        Deselect();
    }

    public void PlayClickSound()
    {
        SoundManager.instance.PlaySingle(SoundManager.instance.buttonSelect2);
    }

    public void Select(Selectable s)
    {
        /*
        if (hasSelected)
            eventSystem.SetSelectedGameObject(null);
        s.Select();
        hasSelected = true;
        */
    }

    public void Deselect()
    {
        if (hasSelected)
            eventSystem.SetSelectedGameObject(null);
        hasSelected = false;
    }

    public void SetTyping(bool t) //for typing
    {
        typing = t;
    }
}
