﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System;


public class LoadSaves : MonoBehaviour
{
    public RectTransform content;
    public GameObject gameInfoPanel;

    private string savePath = "./Saves/";
    private string saveType = ".jlts";

    void Start()
    {
        if (!Directory.Exists(savePath))
            Directory.CreateDirectory(savePath);
        DirectoryInfo saveDir = new DirectoryInfo(savePath);
        FileInfo[] files = saveDir.GetFiles("*" + saveType);

        float buttonDistance = 110f;
        content.sizeDelta = new Vector2(0, 30f + buttonDistance * files.Length);

        for (int i = 0; i < files.Length; ++i)
        {
			if(System.IO.File.Exists(files[i].FullName))
			{
				string[] lines = System.IO.File.ReadAllLines(files[i].FullName);
                GameObject savedGame = Instantiate(gameInfoPanel);
                savedGame.transform.SetParent(content);
                savedGame.transform.localPosition = new Vector3(0f, -65f - buttonDistance * i, 0f);
                savedGame.transform.localScale = Vector3.one;
                savedGame.name = "Save " + i;

                savedGame.transform.FindChild("GameName").gameObject.GetComponent<Text>().text = lines[14];
                
				savedGame.transform.FindChild("UILifeBar").GetComponent<UILifeBar>().open(int.Parse(lines[6]), int.Parse(lines[5]));
                savedGame.transform.FindChild("LastPlayed").GetComponent<Text>().text = "Last played: " + lines[15];
                // Navigation
                Navigation nav = new Navigation();
                nav.mode = Navigation.Mode.Explicit;
                Button thisButton = savedGame.GetComponent<Button>();
                thisButton.navigation = nav;
                if (i > 0)
                {
                    Transform topObj = savedGame.transform.parent.transform.FindChild("Save " + (i - 1));
                    Button topButton = topObj.GetComponent<Button>();
                    nav.selectOnUp = topButton;
                    thisButton.navigation = nav;

                    Navigation topNav = new Navigation();
                    topNav.mode = Navigation.Mode.Explicit;
                    topNav.selectOnUp = topButton.navigation.selectOnUp;
                    topNav.selectOnDown = thisButton;
                    topButton.navigation = topNav;

                    if (i == files.Length - 1) //last saved game
                    {
                        Button backButton = GameObject.Find("BackButton").GetComponent<Button>();
                        nav.selectOnDown = backButton;
                        thisButton.navigation = nav;

                        Navigation backButtonNav = new Navigation();
                        backButtonNav.mode = Navigation.Mode.Explicit;
                        backButtonNav.selectOnUp = thisButton;
                        backButton.navigation = backButtonNav;
                    }
                }
                else
                {
                    GameObject.Find("MenuCanvas").GetComponent<KeyboardUIControl>().defaultSelectable = thisButton;
                }

                savedGame.GetComponent<ScollViewSelectableControl>().scroll =
                    GameObject.Find("SavesScrollView").GetComponent<ScrollRect>();
                savedGame.GetComponent<ScollViewSelectableControl>().smoothScroll =
                    GameObject.Find("SavesScrollView").GetComponent<SmoothScroll>();
        	}
		}

    }
}
