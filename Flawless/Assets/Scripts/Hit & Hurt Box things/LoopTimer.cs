﻿using UnityEngine;
using System.Collections;

public class LoopTimer : MonoBehaviour
{
    public GameObject parent;
    public int appearCount;
    public int disappearCount;
    public int waitCount;
    public float outPosX = 500;
    public float outPosY = 500;
    public bool appearWhileColliding;

    public int active;
    private int counter;
    private float x;
    private float y;
    private int state;
    void Start ()
    {
        x = parent.transform.position.x;
        y = parent.transform.position.y;
        counter = 0;
        state = 0;
        active = 0;
        if (appearCount != 0)
            parent.transform.position = new Vector3(outPosX, outPosY, 0);
        else
            state = 1;
    }

    void Update()
    {
        switch (state)
        {
            case 2:
                if (counter == waitCount)
                {
                    appear();
                }
                break;
            case 1:
                if (counter == disappearCount)
                {
                    state = 2;
                    parent.transform.position = new Vector3(outPosX, outPosY, 0);
                    counter = 0;
                }
                break;
            case 0:
                if (counter == appearCount)
                {
                    appear();
                }
                break;
        }
        counter++;
    }
    private void appear()
    {
        if (active == 0)
        {
            parent.transform.position = new Vector3(x, y, 0);
            Rigidbody2D body = parent.transform.GetComponent<Rigidbody2D>();
            if (body != null)
            {
                body.velocity = Vector2.zero;
            }
            state = 1;
            counter = 0;
        }
        else
            counter--;
    }

    void OnTriggerEnter2D(Collider2D hit)
    {
        if(!appearWhileColliding)
            active++;
    }
    void OnTriggerExit2D(Collider2D hit)
    {
        if (active != 0)
            active--;
    }
}
