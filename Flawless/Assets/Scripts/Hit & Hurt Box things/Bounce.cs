﻿using UnityEngine;
using System.Collections;

public class Bounce : MonoBehaviour
{
    public int timeDown;
    public int timeUp;
    public float distance;
    public float force;

    private bool up = false;
    private int counter;
    private float DistanceUp;
    private float DistanceDown;
    private Rigidbody2D body;

    void Start()
    {
        DistanceUp = distance / timeUp;
        DistanceDown = distance / timeDown;
        body = GetComponent<Rigidbody2D>();
    }
	void Update ()
    {
	    if(up)
        {
            if (body != null)
                body.velocity = new Vector3(0, force, 0);
            else
                transform.position = new Vector3(transform.position.x, transform.position.y + DistanceUp, transform.position.z);
            if (counter == timeUp)
            {
                up = false;
                counter = 0;
            }
            else
                counter++;
        }
        else
        {
            if (body != null)
                body.velocity = new Vector3(0, -force, 0);
            else
                transform.position = new Vector3(transform.position.x,transform.position.y - DistanceDown, transform.position.z);
            if (counter == timeDown)
            {
                up = true;
                counter = 0;
            }
            else
                counter++;
        }
	}
}
