﻿using UnityEngine;
using System.Collections;
public interface HitAble 
{
	void gotHit(Collision2D hit);
    void HitConnected(Collision2D hit);
}
