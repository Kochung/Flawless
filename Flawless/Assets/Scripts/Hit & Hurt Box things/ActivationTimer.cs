﻿using UnityEngine;
using System.Collections;

public class ActivationTimer : MonoBehaviour
{
    public int start;
    public int end;
    public float outPosX=500;
    public float outPosY=500;
    private int counter;
    private float x;
    private float y;
	void Start ()
    {
        x = transform.position.x;
        y = transform.position.y;
        counter = 0;
        if (start != 0)
            transform.position = new Vector3(outPosX, outPosY, 0);
    }
	void Update ()
    {
        if (counter == start)
        {
            transform.position = new Vector3(x, y, 0);
            Rigidbody2D body = transform.GetComponent<Rigidbody2D>();
            if (body != null)
            {
                body.velocity = Vector2.zero;
            }
        }
        if (counter == end)
            GameObject.Destroy(this.gameObject);
        counter++;
	}
}
