﻿using UnityEngine;
using System.Collections;

public class HurtBox : MonoBehaviour
{
    
	private  HitAble parent;
    private Collider2D boxCollider;
    void Awake()
    {
        boxCollider = GetComponent<Collider2D>();
    }
    public void setParent(HitAble actor)
	{
		parent = actor;
	}
	public HitAble getParent()
	{
		return parent;
	}
	void OnCollisionEnter2D(Collision2D hit)
	{
        if(parent != null)
		    parent.gotHit (hit);
	}
    public void off()
    {
        transform.GetComponent<BoxCollider2D>().enabled = false;
    }
    public void setEnable(bool value)
    {
        boxCollider.enabled = value;
    }
}
