﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

public class BlockSnapper : MonoBehaviour
{
    [HideInInspector]
    public float width = 0.48f;
    [HideInInspector]
    public float height = 0.48f;
    [HideInInspector]
    public Color color = Color.white;
    [HideInInspector]
    public Object[] fillPrefabs = new Object[9 + 4 + 3 + 1];
    [HideInInspector]
    public GameObject mapHolder;

    void OnDrawGizmos()
    {
        Vector2 pos = Camera.current.transform.position;
        Gizmos.color = color;

        for (float y = pos.y - 1000f; y < pos.y + 1000f; y += height)
        {
            Gizmos.DrawLine(new Vector3(-1000.0f, Mathf.Floor(y / height) * height),
                            new Vector3(1000.0f, Mathf.Floor(y / height) * height));
        }

        for (float x = pos.x - 1000f; x < pos.x + 1000f; x += width)
        {
            Gizmos.DrawLine(new Vector3(Mathf.Floor(x / width) * width, -1000.0f),
                            new Vector3(Mathf.Floor(x / width) * width, 1000.0f));
        }
    }

}


#if UNITY_EDITOR
public class BlockSnapperWindow : EditorWindow
{
    BlockSnapper snapper;

    public void Init()
    {
        snapper = (BlockSnapper)FindObjectOfType(typeof(BlockSnapper));
    }

    void OnGUI()
    {
        snapper.color = EditorGUILayout.ColorField(snapper.color, GUILayout.Width(200));
    }
}



[CustomEditor(typeof(BlockSnapper))]
public class BlockSnapperEditor : Editor
{
    private BlockSnapper snapper;

    public void OnEnable()
    {
        snapper = (BlockSnapper)target;
        SceneView.onSceneGUIDelegate += BlockSnapperUpdate;
    }
    
    public void OnDisable()
    {
        SceneView.onSceneGUIDelegate -= BlockSnapperUpdate;
    }


    /// <summary>
    /// Draw the map editor's GUI in the Inspector
    /// </summary>
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI(); //probably don't need this

        GUILayout.BeginHorizontal();
        GUILayout.Label("Grid Width");
        snapper.width = EditorGUILayout.FloatField(snapper.width, GUILayout.Width(50));
        if (snapper.width < 0.04f) snapper.width = 0.04f;
        GUILayout.Label("Grid Height");
        snapper.height = EditorGUILayout.FloatField(snapper.height, GUILayout.Width(50));
        if (snapper.height < 0.04f) snapper.height = 0.04f;
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Map Holder");
        snapper.mapHolder = (GameObject)EditorGUILayout.ObjectField(snapper.mapHolder, typeof(Object),
            true, GUILayout.Width(150));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Fill Prefabs");
        GUILayout.EndHorizontal();

        // 0 1 2
        GUILayout.BeginHorizontal();
        for (int i = 0; i < 3; ++i)
        {
            if (snapper.fillPrefabs[i])
            {
                Texture2D previewTexture = AssetPreview.GetAssetPreview(snapper.fillPrefabs[i]);
                GUILayout.Label(previewTexture, GUILayout.Width(25), GUILayout.Height(25));
            }
        }
        for (int i = 0; i < 3; ++i)
            snapper.fillPrefabs[i] = EditorGUILayout.ObjectField(snapper.fillPrefabs[i], typeof(Object), true);
        GUILayout.EndHorizontal();

        // 3 4 5
        GUILayout.BeginHorizontal();
        for (int i = 3; i < 6; ++i)
        {
            if (snapper.fillPrefabs[i])
            {
                Texture2D previewTexture = AssetPreview.GetAssetPreview(snapper.fillPrefabs[i]);
                GUILayout.Label(previewTexture, GUILayout.Width(25), GUILayout.Height(25));
            }
        }
        for (int i = 3; i < 6; ++i)
            snapper.fillPrefabs[i] = EditorGUILayout.ObjectField(snapper.fillPrefabs[i], typeof(Object), true);
        GUILayout.EndHorizontal();

        // 6 7 8
        GUILayout.BeginHorizontal();
        for (int i = 6; i < 9; ++i)
        {
            if (snapper.fillPrefabs[i])
            {
                Texture2D previewTexture = AssetPreview.GetAssetPreview(snapper.fillPrefabs[i]);
                GUILayout.Label(previewTexture, GUILayout.Width(25), GUILayout.Height(25));
            }
        }
        for (int i = 6; i < 9; ++i)
            snapper.fillPrefabs[i] = EditorGUILayout.ObjectField(snapper.fillPrefabs[i], typeof(Object), true);
        GUILayout.EndHorizontal();

        // 9 10
        GUILayout.BeginHorizontal();
        for (int i = 9; i < 11; ++i)
        {
            if (snapper.fillPrefabs[i])
            {
                Texture2D previewTexture = AssetPreview.GetAssetPreview(snapper.fillPrefabs[i]);
                GUILayout.Label(previewTexture, GUILayout.Width(25), GUILayout.Height(25));
            }
        }
        for (int i = 9; i < 11; ++i)
            snapper.fillPrefabs[i] = EditorGUILayout.ObjectField(snapper.fillPrefabs[i], typeof(Object), true);
        GUILayout.EndHorizontal();

        // 11 12
        GUILayout.BeginHorizontal();
        for (int i = 11; i < 13; ++i)
        {
            if (snapper.fillPrefabs[i])
            {
                Texture2D previewTexture = AssetPreview.GetAssetPreview(snapper.fillPrefabs[i]);
                GUILayout.Label(previewTexture, GUILayout.Width(25), GUILayout.Height(25));
            }
        }
        for (int i = 11; i < 13; ++i)
            snapper.fillPrefabs[i] = EditorGUILayout.ObjectField(snapper.fillPrefabs[i], typeof(Object), true);
        GUILayout.EndHorizontal();

        // 13 14 15
        GUILayout.BeginHorizontal();
        for (int i = 13; i < 16; ++i)
        {
            if (snapper.fillPrefabs[i])
            {
                Texture2D previewTexture = AssetPreview.GetAssetPreview(snapper.fillPrefabs[i]);
                GUILayout.Label(previewTexture, GUILayout.Width(25), GUILayout.Height(25));
            }
        }
        for (int i = 13; i < 16; ++i)
            snapper.fillPrefabs[i] = EditorGUILayout.ObjectField(snapper.fillPrefabs[i], typeof(Object), true);
        GUILayout.EndHorizontal();

        // 16           meh I'll just copy and paste
        GUILayout.BeginHorizontal();
        for (int i = 16; i < 17; ++i)
        {
            if (snapper.fillPrefabs[i])
            {
                Texture2D previewTexture = AssetPreview.GetAssetPreview(snapper.fillPrefabs[i]);
                GUILayout.Label(previewTexture, GUILayout.Width(25), GUILayout.Height(25));
            }
        }
        for (int i = 16; i < 17; ++i)
            snapper.fillPrefabs[i] = EditorGUILayout.ObjectField(snapper.fillPrefabs[i], typeof(Object), true);
        GUILayout.EndHorizontal();



        if (GUILayout.Button("Open Grid Color Window", GUILayout.Width(255)))
        {
            BlockSnapperWindow window = (BlockSnapperWindow)EditorWindow.GetWindow(typeof(BlockSnapperWindow));
            window.Init();
        }

        SceneView.RepaintAll();
    }




    /// <summary>
    /// OnSceneUI handler for map editor. Lock the object contains this script in Inspector to use.
    /// </summary>
    /// <param name="sceneview"></param>
    public void BlockSnapperUpdate(SceneView sceneview)
    {
        Event e = Event.current;

        Ray ray = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x, -e.mousePosition.y + Camera.current.pixelHeight));
        Vector3 mousePos = ray.origin;

        if (e.isKey)
        {
            int x = (int)Mathf.Floor(mousePos.x / snapper.width);
            int y = (int)Mathf.Floor(mousePos.y / snapper.height);
            if (e.character == ' ') //ADD BLOCK
            {
                GameObject cur = GameObject.Find(GetBlockName(x, y));
                if (!cur) //empty cell
                {
                    PlaceBlock(x, y);
                    // update other 8 tiles
                    for (int r = -1; r <= 1; ++r)
                        for (int c = -1; c <= 1; ++c)
                            if (r != 0 || c != 0)
                                UpdateOldBlock(x + c, y + r);
                }
            }
            else if (e.character == 'd') //DELETE TILE
            {
                GameObject cur = GameObject.Find(GetBlockName(x, y));
                if (cur)
                {
                    DestroyImmediate(cur);
                    // update other 8 tiles
                    for (int r = -1; r <= 1; ++r)
                        for (int c = -1; c <= 1; ++c)
                            if (r != 0 || c != 0)
                                UpdateOldBlock(x + c, y + r);
                }
                else
                {
                    cur = GameObject.Find(GetPlatformName(x, y));
                    if (cur)
                    {
                        DestroyImmediate(cur);
                        // update other 2 tiles
                        UpdateOldPlatform(x - 1, y);
                        UpdateOldPlatform(x + 1, y);
                    }
                }
            }
            else if (e.character == 'a') //ADD PLATFORM
            {
                GameObject cur = GameObject.Find(GetPlatformName(x, y));
                if (!cur) //empty cell
                {
                    PlacePlatform(x, y);
                    // update other 2 tiles
                    UpdateOldPlatform(x - 1, y);
                    UpdateOldPlatform(x + 1, y);
                }
            }
        }
    }

    #region GridUpdateHelperMethods
    private string GetBlockName(int x, int y)
    {
        return "Block[" + x + "," + y + "]";
    }

    private string GetPlatformName(int x, int y)
    {
        return "Platf[" + x + "," + y + "]";
    }

    private int FindTileId(int x, int y)
    {
        GameObject top = GameObject.Find(GetBlockName(x, y + 1));
        GameObject bottom = GameObject.Find(GetBlockName(x, y - 1));
        GameObject left = GameObject.Find(GetBlockName(x - 1, y));
        GameObject right = GameObject.Find(GetBlockName(x + 1, y));
        GameObject topLeft = GameObject.Find(GetBlockName(x - 1, y + 1));
        GameObject topRight = GameObject.Find(GetBlockName(x + 1, y + 1));
        GameObject bottomLeft = GameObject.Find(GetBlockName(x - 1, y - 1));
        GameObject bottomRight = GameObject.Find(GetBlockName(x + 1, y - 1));

        if (top && left && bottom && right) //4 9 10 11 12
        {
            if (!topLeft) return 9 + 0;
            else if (!topRight) return 9 + 1;
            else if (!bottomLeft) return 9 + 2;
            else if (!bottomRight) return 9 + 3;
            else return 4;
        }
        else
        {
            if (!top)
            { //0 1 2
                if (left && right) return 1;
                else if (left) return 2;
                else return 0;
            }
            else if (!bottom)
            { //6 7 8
                if (left && right) return 7;
                else if (left) return 8;
                else return 6;
            }
            else
            { //3 5
                return left ? 5 : 3;
            }
        }
    }

    private int FindPlatformId(int x, int y) //13 14 15  16
    {
        GameObject left = GameObject.Find(GetPlatformName(x - 1, y));
        GameObject right = GameObject.Find(GetPlatformName(x + 1, y));
        if (left && right) return 14;
        else if (left) return 15;
        else if (right) return 13;
        return 16;
    }

    private void PlacePlatform(int x, int y)
    {
        GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(snapper.fillPrefabs[FindPlatformId(x, y)]);
        if (obj == null) return;
        obj.name = GetPlatformName(x, y);
        Vector3 aligned = new Vector3(x * snapper.width + snapper.width / 2f, y * snapper.height + snapper.height / 2f);
        obj.transform.position = aligned;
        obj.transform.SetParent(snapper.mapHolder.transform);
    }

    private void UpdateOldPlatform(int x, int y)
    {
        GameObject cur = GameObject.Find(GetPlatformName(x, y));
        if (cur)
        {
            DestroyImmediate(cur);
            PlacePlatform(x, y);
        }
    }

    private void PlaceBlock(int x, int y)
    {
        GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(snapper.fillPrefabs[FindTileId(x, y)]);
        if (obj == null) return;
        obj.name = GetBlockName(x, y);
        Vector3 aligned = new Vector3(x * snapper.width + snapper.width / 2f, y * snapper.height + snapper.height / 2f);
        obj.transform.position = aligned;
        obj.transform.SetParent(snapper.mapHolder.transform);
    }

    private void UpdateOldBlock(int x, int y)
    {
        GameObject cur = GameObject.Find(GetBlockName(x, y));
        if (cur)
        {
            DestroyImmediate(cur);
            PlaceBlock(x, y);
        }
    }
    #endregion
}
#endif