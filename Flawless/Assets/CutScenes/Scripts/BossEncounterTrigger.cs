﻿using UnityEngine;
using System;
using System.Collections;

public class BossEncounterTrigger : MonoBehaviour
{
    public Camera cam;
    public float dialogSwitchSpeed = 0.5f;

    private bool activated = false;
    private GameObject blockDoor;
    private CutScenesBoss boss = null;

    private string[][] playerMessages;
    private string[][] bossMessages = new string[][] {
        new string[]{ "AWL:" + Environment.NewLine + "... must get to the core..." },
        new string[]{ "AWL:" + Environment.NewLine + "Human?", "AWL:" + Environment.NewLine + "Why should I care about those low-lifes?" },
        new string[]{ "AWL:" + Environment.NewLine + "You are no better.", "AWL:" + Environment.NewLine + "I'm wasting time here.","I have more important thing to do."}
    };

    void Start()
    {
        playerMessages = new string[][] {
            new string[]{ GameManager.instance.fileName + ":" + Environment.NewLine + "The core?!", GameManager.instance.fileName + ":" + Environment.NewLine + "You know where my father is?" },
            new string[]{ GameManager.instance.fileName + ":" + Environment.NewLine + "Don't you dare say that about my father!!!" }};
        blockDoor = GameObject.Find("BlockDoor");
        if (blockDoor) blockDoor.SetActive(false);

        GameObject bossObj = GameObject.Find("Boss");
        if (bossObj)
        {
            boss = bossObj.GetComponent<CutScenesBoss>();
  //          if (boss) boss.enabled = false; //disable BombAwl script
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (activated) return;

        if (other.CompareTag("Player"))
        {
            // Enter cut scene ('disable' Player.Update())
            GameSettings.instance.cutScene = true;
            // Disable camera follow
            cam.GetComponent<CameraFollow>().enabled = false;
            // Stop camera movement
            cam.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            StartCoroutine(RunCutScene());

            activated = true; //don't trigger again
        }
    }

    IEnumerator RunCutScene()
    {
        // Slowly move camera (constant 1 second)
        yield return StartCoroutine(MoveCamera(4.5f, 1f));
        // Shut the door
        if (blockDoor) blockDoor.SetActive(true);
        yield return new WaitForSeconds(0.5f * dialogSwitchSpeed);

        // Chit chat
        for (int i = 0; i < 2; ++i)
        {
            yield return StartCoroutine(MoveCamera(1.5f + (i > 0 ? 0.75f : 0f), dialogSwitchSpeed));
            GameManager.instance.sendTextBoxMessage(bossMessages[i]);

            yield return StartCoroutine(MoveCamera(-2.25f, dialogSwitchSpeed));
            GameManager.instance.sendTextBoxMessage(playerMessages[i]);
        }
        // Boss says bye
        yield return StartCoroutine(MoveCamera(2.25f, dialogSwitchSpeed));
        GameManager.instance.sendTextBoxMessage(bossMessages[2]);

        // Boss runs to portal
        if (boss)
        {
            boss.moveable.moveX(2f * boss.moveSpeed);
            boss.moveable.flip(boss.transform);
            boss.animator.SetBool("Run", true);
        }
        // Move camera back to player
        yield return StartCoroutine(MoveCamera(-1.5f, dialogSwitchSpeed));

        // Smooth movement before re-enable camera follow
        yield return StartCoroutine(MoveCamera(-4.35f, 2 * dialogSwitchSpeed)); //magic -4.35f
        // Re-enable camera follow
        cam.GetComponent<CameraFollow>().enabled = true;
        // You can move now. TO THE BATTLE!!!
        GameSettings.instance.cutScene = false;
    }

    IEnumerator MoveCamera(float distance, float camMovingTime)
    {
        float lerpPercentage = 0;
        while (lerpPercentage <= 1.0f)
        {
            float deltaX = distance * Time.deltaTime / camMovingTime;
            cam.transform.position += Vector3.right * deltaX;
            lerpPercentage += Time.deltaTime / camMovingTime;
            yield return new WaitForFixedUpdate();
        }
    }
}
