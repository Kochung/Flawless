﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class EndGameTeaser : MonoBehaviour
{
    public Camera cam;
    public GameObject boss;

    private Rigidbody2D camRb2d;
    private Player player;
    private Image whiteScreen; //inside UICanvas

    private string[][] playerMessages;
    private string[][] bossMessages = new string[][] {
        new string[]{ "JLTS:"+Environment .NewLine+"This place has no used for me anymore.", "JLTS:"+Environment .NewLine+"I will rebuild it." }
    };

    void Awake()
    {
        camRb2d = cam.GetComponent<Rigidbody2D>();
        player = GameObject.Find("Player").GetComponent<Player>();
        whiteScreen = GameObject.Find("WhiteScreen").GetComponent<Image>();
    }

    void Start()
    {
        // Enter cut scene ('disable' Player.Update())
        GameSettings.instance.cutScene = true;
        // Disable camera follow
        cam.GetComponent<CameraFollow>().enabled = false;
        // Stop camera movement
        cam.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        playerMessages = new string[][] {
            new string[]{ GameManager.instance.fileName+":"+Environment .NewLine+"What's happening?" }};
        StartCoroutine(RunCutScene());
    }

    IEnumerator RunCutScene()
    {
        // Wait for everything to settle down
        yield return new WaitForSeconds(0.25f);

        // Ground shake, player starts asking why
        yield return StartCoroutine(GroundShake(-1f, 0f, 1f, 0.02f));
        GameManager.instance.sendTextBoxMessage(playerMessages[0]);
        // JLTS moves in & has some words
        yield return StartCoroutine(BossMoveIn(2.7f, 0.02f, Vector3.left * 0.066f));
        GameManager.instance.sendTextBoxMessageNoPause(bossMessages[0]);
        while (GameManager.instance.textBoxIsActive())
        {
            if (GameManager.instance.textBoxIsActive())
                yield return new WaitForSeconds(0.25f);
        }
        // JLTS flies away
        StartCoroutine(GroundShake(-1f, 0f, 2f, 0.02f));
        yield return StartCoroutine(BossMoveIn(2.5f, 0.02f, Vector3.up * 0.05f));

        // Ground shake even move
        StartCoroutine(GroundShake(-2f, 5f, 100f, 0.02f));


        // Player automatically jumps down the hole and escapes
        yield return new WaitForSeconds(0.75f);
        GameSettings.instance.cutSceneRunning = -1;

        // Screen white out
        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(WhiteOutScreen());

        // Wait for few seconds then switch to credit scene
        yield return new WaitForSeconds(5f);
        Application.LoadLevel("SaveNewGamePlus");
        SoundManager.instance.PlayMusic("main");
    }

    IEnumerator WhiteOutScreen()
    {
        float alpha = 0.015625f;
        for (int i = 0; i < 64; ++i, alpha += 0.015625f)
        {
            whiteScreen.color = new Color(1f, 1f, 1f, alpha);
            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator BossMoveIn(float duration, float interval, Vector3 lerpVect)
    {
        while (duration > 0)
        {
            boss.transform.localPosition += lerpVect;
            duration -= interval;
            Debug.Log("bossMove duration left = " + duration);
            yield return new WaitForSeconds(interval);
        }
    }

    IEnumerator GroundShake(float camShakeY, float addedCamShakeX, float duration, float interval)
    {
        Vector3 camOriginalPos = cam.transform.localPosition;
        // Shake camera
        while (duration > 0)
        {
            float camShakeX =(float)(GameManager.instance.random.NextDouble() * 6 - 3);
            camShakeX = camShakeX + Mathf.Sign(camShakeX) * Mathf.Abs(addedCamShakeX);
            camRb2d.velocity = new Vector2(camShakeX, camShakeY);
            if (cam.transform.localPosition.y > 0.5f) //cam shakes too high
                camShakeY = -Mathf.Abs(camShakeY);
            else
                camShakeY = -camShakeY;
            duration -= interval;
            yield return new WaitForSeconds(interval);
        }
        // Reset camera after shaking
        camRb2d.velocity = Vector2.zero;
        cam.transform.localPosition = camOriginalPos;
    }
}
