﻿using UnityEngine;
using System;
using System.Collections;

public class BossDeath : MonoBehaviour
{
    public Camera cam;
    public float dialogSwitchSpeed = 0.5f;
    public float minCamLeftPos, maxCamRightPos; //to prevent camera from moving out of bounds
    public GameObject exitDoor;

    private bool activated = false;
    private CutScenesBoss boss = null;

    private string[][] bossMessages = new string[][] {
        new string[]{
            "AWL:" + Environment.NewLine + "I have failed...",
            "AWL:" + Environment.NewLine + "... but at least... I have slowed down... J.L.T.S.' plan...",
            "AWL:" + Environment.NewLine + "We... will.. not... stop..."
        }
    };

    void Start()
    {
        GameObject bossObj = GameObject.Find("Boss");
        if (bossObj)
        {
            boss = bossObj.GetComponent<CutScenesBoss>();
            if (boss) boss.enabled = false; //disable BombAwl script
        }
    }

    void Update()
    {
        if (activated) return;
        EnterCutScene();
        activated = true; //don't trigger again
    }

    public void EnterCutScene()
    {
        // Enter cut scene ('disable' Player.Update())
        GameSettings.instance.cutScene = true;
        // Disable camera follow
        cam.GetComponent<CameraFollow>().enabled = false;
        // Stop camera movement
        cam.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        StartCoroutine(RunCutScene());
    }

    IEnumerator RunCutScene()
    {
        // Calculate distance from cam to boss
        float bossX = boss.transform.localPosition.x;
        float distanceToBoss = cam.transform.localPosition.x - bossX;
        // Move cam to boss
        yield return StartCoroutine(MoveCamera(-distanceToBoss, 1f));
        // Start dying messages
        GameManager.instance.sendTextBoxMessage(bossMessages[0]);
        // Destroy boss
        boss.spriterenderer.enabled = false;
        yield return StartCoroutine(QuickenBlinkingSprite(boss.spriterenderer, 0.5f, 0.85f, 20)); //3.22 - 0.5 = 2.72s
        DestroyObject(GameObject.Find("Boss"));
        boss = null;
        // Spawn boss chest
        //////////// I'll just use double jump chest here (drag & drop in unity editor) ////////////
        yield return new WaitForSeconds(1f);
        // Also spawn exit portal, which leads to summary scene OR JLTS teaser scene
        Vector2 doorPos = new Vector2(-14.13f, -14.79f); //-1.7f: door will stay on ground
        // Move cam to exit portal position
        float distanceToExit = cam.transform.localPosition.x - doorPos.x;
        yield return StartCoroutine(MoveCamera(-distanceToExit, 1f));
        // Instantiate exit portal
        yield return new WaitForSeconds(0.5f); //wait 0.5s then spawn portal
        GameObject door = Instantiate(exitDoor) as GameObject;
        door.transform.localPosition = doorPos;
        // Additional scripts for portal here...
        //
        //
        yield return new WaitForSeconds(1.5f);

        // Move cam back to player
        float distanceToPlayer = cam.transform.localPosition.x - GameObject.Find("Player").transform.localPosition.x;
        float camMoveTime = 2f;
        if (Mathf.Abs(distanceToPlayer) < 5f)
            camMoveTime = 1f;
        yield return StartCoroutine(MoveCamera(-distanceToPlayer, camMoveTime));

        // End cutscene. GET. THE. LOOT. AND. GET. OUT.
        cam.GetComponent<CameraFollow>().enabled = true;
        GameSettings.instance.cutScene = false;
    }


    IEnumerator QuickenBlinkingSprite(SpriteRenderer rend, float startBlinkingInterval, float quickenPercentage, int blinkCount)
    {
        while (blinkCount-->0)
        {
            rend.enabled = !rend.enabled;
            if (boss) boss.enabled = false; //fix the boss small movement towards player
            yield return new WaitForSeconds(startBlinkingInterval *= quickenPercentage);
        }
    }

    IEnumerator MoveCamera(float distance, float camMovingTime)
    {
        float lerpPercentage = 0;
        while (lerpPercentage <= 1.0f)
        {
            float deltaX = distance * Time.deltaTime / camMovingTime;
            cam.transform.position += Vector3.right * deltaX;
            if (   cam.transform.localPosition.x > maxCamRightPos
                || cam.transform.localPosition.x < minCamLeftPos)
                cam.transform.position -= Vector3.right * deltaX; //undo if cam moves out of bounds
            lerpPercentage += Time.deltaTime / camMovingTime;
            yield return new WaitForFixedUpdate();
        }
    }
}
